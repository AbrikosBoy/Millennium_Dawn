leader_traits = {
	i_am_the_state = {
		random = no
		stability_factor = 0.1
		corruption_cost_factor = 1
		economic_cycles_cost_factor = 0.25

		ai_will_do = {
			factor = 1
		}
	}
	ccm_elite = {
		random = yes
		corruption_cost_factor = 0.05
		political_power_gain = 0.05

		ai_will_do = {
			factor = 1
		}
	}
	moi_puppet = {
		random = no
		political_power_factor = -0.10

		ai_will_do = {
			factor = 1
		}
	}
	enviromentalist = {
		random = yes

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_kikuyu = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_luhya = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_kalenjin = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_luo = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_kamba = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_kisii = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_mijikenda = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_somali = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	ethnic_others = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	anti_kenyatta = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}

	anti_odinga = {
		random = no

		ai_will_do = {
			factor = 1
		}
	}
	tribalist = {
		random = no
		corruption_cost_factor = 0.1
		political_power_gain = 0.1
		stability_factor = -0.03

		ai_will_do = {
			factor = 1
		}
	}
}