focus_tree = {

	id = siam_focus

	country = {
		factor = 0
		modifier = {
			tag = SIA
			add = 21
		}
	}
	# ASEAN Shared Focus Tree
	shared_focus = ASEAN_asean

	##CENTRAL FOCUS##
	focus = {
		id = SIA_state_of_affairs
		icon = treaty2
		x = 9
		y = 0
		cost = 5

		search_filters = { FOCUS_FILTER_POLITICAL }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_state_of_affairs"
			add_political_power = 50
		}

		ai_will_do = {
			factor = 1
		}
	}

	focus = {
		id = SIA_address_the_financial_crisis
		icon = industry_democratic
		x = 2
		y = 1
		cost = 10

		prerequisite = {
			focus = SIA_state_of_affairs
		}
		search_filters = {FOCUS_FILTER_POLITICAL }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_address_the_financial_crisis"
			add_political_power = 50
			custom_effect_tooltip = SIA_resolving_crisis_tt

		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_continue_austerity_measures
		icon = gold_standard
		x = 1
		y = 2
		cost = 10

		prerequisite = {
		focus = SIA_address_the_financial_crisis
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_continue_austerity_measures"
			add_political_power = -50
			if = {
				limit = { has_idea = SIA_financial_crisis }
				swap_ideas = {
					remove_idea = SIA_financial_crisis
					add_idea = SIA_resolving_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_resolving_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_remnants_crisis
				}
				calculate_tax_gain = yes
			}
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = 5
					}
				}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_ask_for_assistance_from_the_imf
		icon = authoritarian_democracy
		x = 3
		y = 2
		cost = 10

		prerequisite = {
		focus = SIA_address_the_financial_crisis
		}

		mutually_exclusive = {
		focus = SIA_tread_the_route_of_no_return
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_ask_for_assistance_from_the_imf"
			custom_effect_tooltip = 11_aid_tt
			add_to_variable = { treasury = 15 }
			custom_effect_tooltip = 11_BRM_decrease_domestic_influence_tt
			hidden_effect = {
				subtract_from_variable = { domestic_influence_amount = 10 }
				recalculate_influence = yes
				}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_reform_land_use
		icon = agriculture
		x = 0
		y = 3
		cost = 10

		prerequisite = {
		focus = SIA_continue_austerity_measures
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_INDUSTRY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_reform_land_use"
			add_ideas = { SIA_reformed_land }
			decrease_landowners_opinion = yes
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_internal_security
		icon = military_sphere
		x = 9
		y = 1
		cost = 10

		prerequisite = {
		focus = SIA_state_of_affairs
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_state_of_affairs"

			add_political_power = 10
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_an_iron_fist
		icon = political_pressure
		x = 9
		y = 2
		cost = 10

		prerequisite = {
		focus = SIA_internal_security
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY}
		mutually_exclusive = {
		focus = SIA_a_gentle_hand
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_an_iron_fist"
			add_political_power = -50
			add_stability = -0.05
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
		}

		ai_will_do = {
			factor = 0
			modifier = {
				add = 1
				is_historical_focus_on = no
			}
		}
	}

		focus = {
		id = SIA_tread_the_route_of_no_return
		icon = colonial_crackdown
		x = 7
		y = 2
		cost = 10

		prerequisite = {
		focus = SIA_internal_security
		}
		search_filters = {FOCUS_FILTER_POLITICAL }
		mutually_exclusive = {
		focus = SIA_ask_for_assistance_from_the_imf
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_internal_security"
			add_political_power = 50
			recalculate_party = yes
				add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
			custom_effect_tooltip = SIA_increase_domestic_influence_tt
			hidden_effect = {
				add_to_variable = { domestic_influence_amount = 20 }
				recalculate_influence = yes
			}
			custom_effect_tooltip = SIA_warning_slippery_slope_tt
		}

		ai_will_do = {
			factor = 0
			modifier = {
				add = 1
				is_historical_focus_on = no
			}
		}
	}

		focus = {
		id = SIA_a_gentle_hand
		icon = concessions2
		x = 11
		y = 2
		cost = 10

		prerequisite = {
		focus = SIA_internal_security
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY}
		mutually_exclusive = {
		focus = SIA_an_iron_fist
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_a_gentle_hand"
			add_political_power = 50
			add_stability = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_cspmk
		icon = home_defense
		x = 15
		y = 1
		cost = 5

		prerequisite = {
		focus = SIA_state_of_affairs
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_cspmk"
			add_political_power = 100
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_free_man
		icon = democracy3
		x = 13
		y = 2
		cost = 5

		prerequisite = {
		focus = SIA_cspmk
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_free_man"
			add_political_power = 50
			add_stability = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_acp
		icon = military_mission
		x = 15
		y = 2
		cost = 5

		prerequisite = {
		focus = SIA_cspmk
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_cspmk"
			add_political_power = 50
			add_war_support = 0.1
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_embrace_thai_culture
		icon = television
		x = 17
		y = 2
		cost = 5

		prerequisite = {
		focus = SIA_cspmk
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_cspmk"
			add_political_power = 50
			add_stability = 0.05
			add_ideas = { SIA_embraced_culture }
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_investigate_rampant_corruption
		icon = aristocratic_funding
		x = 2
		y = 3
		cost = 10

		prerequisite = {
		focus = SIA_continue_austerity_measures
		}
		prerequisite = {
		focus = SIA_ask_for_assistance_from_the_imf
		}
		search_filters = {FOCUS_FILTER_CORRUPTION}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_investigate_rampant_corruption"
			decrease_corruption = yes
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -5
					}
				}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_allow_corruption
		icon = union_negotiations
		x = 6
		y = 3
		cost = 10

		prerequisite = {
		focus = SIA_tread_the_route_of_no_return
		}
		prerequisite = {
		focus = SIA_an_iron_fist
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_allow_corruption"
			add_political_power = 100
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_cease_provisions
		icon = flames_of_rebellion
		x = 8
		y = 3
		cost = 10

		prerequisite = {
		focus = SIA_tread_the_route_of_no_return
		}
		prerequisite = {
		focus = SIA_an_iron_fist
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_cease_provisions"
			add_ideas = { Western_Sanctions }
			add_political_power = 100
			custom_effect_tooltip =  2_SIA_increase_domestic_influence_tt
			hidden_effect = {
				add_to_variable = { domestic_influence_amount = 40 }
				recalculate_influence = yes
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_crackdown_drug_trade
		icon = anti_communism
		x = 10
		y = 3
		cost = 10

		prerequisite = {
		focus = SIA_tread_the_route_of_no_return
		}
		prerequisite = {
		focus = SIA_an_iron_fist
		}
		search_filters = {FOCUS_FILTER_STABILITY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_tread_the_route_of_no_return"
			add_stability = 0.05
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -5
					}
				}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_review_drug_policy
		icon = pass_legislation
		x = 12
		y = 3
		cost = 10

		prerequisite = {
		focus = SIA_a_gentle_hand
		}
		search_filters = {FOCUS_FILTER_STABILITY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_review_drug_policy"
			add_stability = 0.05
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -5
					}
				}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_role_of_media
		icon = liberal_press
		x = 14
		y = 3
		cost = 5

		prerequisite = {
		focus = SIA_embrace_thai_culture
		}
		prerequisite = {
		focus = SIA_acp
		}
		prerequisite = {
		focus = SIA_free_man
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_role_of_media"
			add_political_power = 50
			news_event = {
					id = thailand.1
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_unite_ethnic_groups
		icon = religious_partnership_catholic_sunni
		x = 16
		y = 3
		cost = 5

		prerequisite = {
		focus = SIA_embrace_thai_culture
		}
		prerequisite = {
		focus = SIA_acp
		}
		prerequisite = {
		focus = SIA_free_man
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_unite_ethnic_groups"
			add_political_power = 50
			add_stability = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_business_bail
		icon = consumer_goods
		x = 1
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_investigate_rampant_corruption
		}
		search_filters = {FOCUS_FILTER_INDUSTRY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_business_bail"
			add_ideas = { SIA_assisted_business }
			increase_small_medium_business_owners_opinion = yes
			custom_effect_tooltip = 10_billion_expense_tt
			hidden_effect =
			{ add_to_variable = { treasury = -10 } 	}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_implement_healthcare
		icon = concessions
		x = 3
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_investigate_rampant_corruption
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_implement_healthcare"
			if = {
				limit = { has_idea = health_01 }
				swap_ideas = {
					remove_idea = health_01
					add_idea = health_02
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = health_02 }
				swap_ideas = {
					remove_idea = health_02
					add_idea = health_03
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = health_03 }
				swap_ideas = {
					remove_idea = health_03
					add_idea = health_04
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = health_04 }
				swap_ideas = {
					remove_idea = health_04
					add_idea = health_05
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = health_05 }
				swap_ideas = {
					remove_idea = health_05
					add_idea = health_06
				}
				calculate_tax_gain = yes
			}
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -5
					}
		}

		ai_will_do = {
			factor = 1
			}
		}
	}

		focus = {
		id = SIA_nationalize_companies
		icon = industry_civilian
		x = 7
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_allow_corruption
		}
		prerequisite = {
		focus = SIA_cease_provisions
		}
		prerequisite = {
		focus = SIA_crackdown_drug_trade
		}
		search_filters = {FOCUS_FILTER_POLITICAL FOCUS_FILTER_INDUSTRY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_nationalize_companies"
			add_political_power = 50
			add_stability = -0.1
			recalculate_party = yes
				add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
			add_ideas = { SIA_nationalized_corporations }
			if = {
				limit = { has_idea = SIA_financial_crisis }
				swap_ideas = {
					remove_idea = SIA_financial_crisis
					add_idea = SIA_resolving_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_resolving_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_remnants_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_remnants_crisis }
				swap_ideas = {
					remove_idea = SIA_remnants_crisis
					add_idea = SIA_resolving_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_resolving_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_resolving_crisis2
				}
				calculate_tax_gain = yes
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_expand_police
		icon = general_staff
		x = 9
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_allow_corruption
		}
		prerequisite = {
		focus = SIA_cease_provisions
		}
		prerequisite = {
		focus = SIA_crackdown_drug_trade
		}
		search_filters = {FOCUS_FILTER_STABILITY FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_expand_police"
			if = {
				limit = { has_idea = police_01 }
				swap_ideas = {
					remove_idea = police_01
					add_idea = police_02
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = police_02 }
				swap_ideas = {
					remove_idea = police_02
					add_idea = police_03
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = police_03 }
				swap_ideas = {
					remove_idea = police_03
					add_idea = police_04
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = police_04 }
				swap_ideas = {
					remove_idea = police_04
					add_idea = police_05
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = police_05 }
				swap_ideas = {
					remove_idea = police_05
					add_idea = police_06
				}
				calculate_tax_gain = yes
			}
			add_stability = 0.10
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -5
					}
		}
	}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_reform_immigration
		icon = treaty
		x = 11
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_review_drug_policy
		}
		search_filters = {FOCUS_FILTER_STABILITY FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_reform_immigration"
			add_stability = 0.05
			custom_effect_tooltip = 5_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -5
					}
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_accomodate_demands
		icon = improve_relations
		x = 13
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_review_drug_policy
		}
		search_filters = {FOCUS_FILTER_STABILITY FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_accomodate_demands"
			add_stability = 0.1
			add_popularity = {
				ideology = democratic
				popularity = 0.05
			}
			add_popularity = {
				ideology = neutrality
				popularity = 0.05
			}
			custom_effect_tooltip = 10_billion_expense_tt
			hidden_effect = {
				add_to_variable = {
					treasury = -10
					}
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_revitalize_arts
		icon = global_community
		x = 15
		y = 4
		cost = 10

		prerequisite = {
		focus = SIA_role_of_media
		}
		prerequisite = {
		focus = SIA_unite_ethnic_groups
		}
		search_filters = {FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_revitalize_arts"
			subtract_from_variable = { treasury = 1.5 }
			custom_effect_tooltip = 1_5_billion_expense_tt
			add_political_power = 100
			add_war_support = 0.1
			every_other_country = {
				limit = {
					NOT = {
						has_idea = Non_State_Actor
						has_idea = Lacks_International_Recognition
						has_idea = rival_government
					}
				}
				hidden_effect = {
						set_temp_variable = { influence_gain = 2 }
						for_loop_effect = {
							end = influence_array^num
							value = v
							if = {
								limit = {
									check_variable = { influence_array^v = SIA }
								}
								add_to_variable = { influence_array_val^v = influence_gain }
								set_country_flag = found
							}
						}
						if = {
							limit = { NOT = { has_country_flag = found } }
								add_to_array = { influence_array = SIA.id }
								add_to_array = { influence_array_val = influence_gain }
						}
						clr_country_flag = found
						recalculate_influence = yes
				}
				custom_effect_tooltip = SIA_small_influence_tt
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_reduce_poverty
		icon = agriculture2
		x = 1
		y = 5
		cost = 10

		prerequisite = {
		focus = SIA_business_bail
		}
		prerequisite = {
		focus = SIA_implement_healthcare
		}
		search_filters = {FOCUS_FILTER_INDUSTRY FOCUS_FILTER_STABILITY FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_reduce_poverty"
			add_political_power = 50
			add_stability = 0.05
			hidden_effect =
			{ add_to_variable = { treasury = -5 } 	}
			if = {
				limit = { has_idea = SIA_the_divide }
				swap_ideas = {
					remove_idea = SIA_the_divide
					add_idea = SIA_the_divide2
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_the_divide1 }
				swap_ideas = {
					remove_idea = SIA_the_divide1
					add_idea = SIA_the_divide
				}
				calculate_tax_gain = yes
			}
			506 = {
				add_building_construction = {
					type = infrastructure
					level = 3
					instant_build = yes
				}
			}
			custom_effect_tooltip = 5_billion_expense_tt
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_revitalize_industrial
		icon = production
		x = 3
		y = 5
		cost = 10

		prerequisite = {
		focus = SIA_business_bail
		}
		prerequisite = {
		focus = SIA_implement_healthcare
		}
		search_filters = {FOCUS_FILTER_INDUSTRY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_revitalize_industrial"
			if = {
				limit = { has_idea = SIA_financial_crisis }
				swap_ideas = {
					remove_idea = SIA_financial_crisis
					add_idea = SIA_resolving_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_resolving_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_remnants_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_remnants_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_recovery_crisis
				}
				calculate_tax_gain = yes
			}
			random_owned_controlled_state = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 2
					instant_build = yes
				}
			}
			hidden_effect =
			{ add_to_variable = { treasury = -10 } 	}
			custom_effect_tooltip = 10_billion_expense_tt
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_subsidize_tongkah
		icon = construction2
		x = 0
		y = 6
		cost = 10

		prerequisite = {
		focus = SIA_reduce_poverty
		}
		prerequisite = {
		focus = SIA_revitalize_industrial
		}
		search_filters = {FOCUS_FILTER_INDUSTRY}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_subsidize_tongkah"
			add_ideas = { SIA_tongkah_profits }
			hidden_effect =
			{ add_to_variable = { treasury = -25 } 	}
			509 = {
				add_resource = {
					type = steel
					amount = 10
				}
				add_resource = {
					type = tungsten
					amount = 5
				}
			}
			custom_effect_tooltip = 25_billion_expense_tt
		}

		ai_will_do = {
			factor = 1
		}
	}

		focus = {
		id = SIA_prosperity
		icon = money
		x = 2
		y = 6
		cost = 10

		prerequisite = {
		focus = SIA_reduce_poverty
		}
		prerequisite = {
		focus = SIA_revitalize_industrial
		}
		search_filters = {FOCUS_FILTER_INDUSTRY FOCUS_FILTER_STABILITY FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_prosperity"
			add_political_power = 100
			add_stability = 0.1
			if = {
				limit = { has_idea = SIA_financial_crisis }
				swap_ideas = {
					remove_idea = SIA_financial_crisis
					add_idea = SIA_resolving_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_resolving_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_remnants_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_remnants_crisis }
				swap_ideas = {
					remove_idea = SIA_resolving_crisis
					add_idea = SIA_recovery_crisis
				}
				calculate_tax_gain = yes
			}
			else_if = {
				limit = { has_idea = SIA_recovery_crisis }
				swap_ideas = {
					remove_idea = SIA_recovery_crisis
					add_idea = SIA_recovery_crisis2
				}
				calculate_tax_gain = yes
			}
			hidden_effect =
			{ add_to_variable = { treasury = -10 } 	}
			custom_effect_tooltip = 10_billion_expense_tt
		}

		ai_will_do = {
			factor = 1
		}
	}

	focus = {
		id = SIA_state_capitalism
		icon = mobilize_the_workers
		x = 7
		y = 5
		cost = 10

		prerequisite = {
		focus = SIA_nationalize_companies
		}
		prerequisite = {
		focus = SIA_expand_police
		}
		search_filters = {FOCUS_FILTER_INDUSTRY FOCUS_FILTER_POLITICAL}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SIA_state_capitalism"
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
			add_ideas = { SIA_statecapitalism }
			if = {
				limit = { has_idea = SIA_nationalized_corporations }
				swap_ideas = {
					remove_idea = SIA_nationalized_corporations
					add_ideas =  SIA_statecapitalism
					}
				calculate_tax_gain = yes
				}
			hidden_effect =
			{ add_to_variable = { treasury = -10 } 	}
			custom_effect_tooltip = 10_billion_expense_tt
		}

		ai_will_do = {
			factor = 1
		}
	}




}