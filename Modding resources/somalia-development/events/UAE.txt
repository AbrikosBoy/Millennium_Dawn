﻿add_namespace = uae
add_namespace = UAE_News

#Offer to finance puntlands police force
country_event = {
	id = uae.1
	title = uae.1.t
	desc = uae.1.d
	picture = GFX_puntland_police_force_ev

	trigger = {
		tag = UAE
		PUN = {
			focus_progress = {
				focus = puntland_maritime_police
				progress > 0.3
			}
		}
	}

	fire_only_once = yes

	option = {	# Small finance
		name = uae.1.a
		log = "[GetDateText]: [This.GetName]: uae.1.a executed"
		add_equipment_to_stockpile = { type = infantry_weapons amount = -500 }
		PUN = { country_event = { id = puntland.49 } set_country_flag = PMPF_uae_help1 }
		ai_chance = {
			factor = 90
		}
	}
	option = {	# Big finance help
		name = uae.1.b
		log = "[GetDateText]: [This.GetName]: uae.1.b executed"
		effect_tooltip = { add_equipment_to_stockpile = { type = infantry_weapons amount = -500 } }
		PUN = { country_event = { id = puntland.49 } set_country_flag = PMPF_uae_help2 }
		custom_effect_tooltip = UAE_pun_ships_1
		ai_chance = {
			factor = 10
		}
	}
	option = {	# Extensive financial help
		name = uae.1.c
		log = "[GetDateText]: [This.GetName]: uae.1.c executed"
		add_equipment_to_stockpile = { type = infantry_weapons amount = -500 }
		PUN = { country_event = { id = puntland.49 } set_country_flag = PMPF_uae_help3 }
		custom_effect_tooltip = UAE_pun_ships_2
		ai_chance = {
			factor = 90
		}
	}
	option = {	# dont help
		name = uae.1.e
		log = "[GetDateText]: [This.GetName]: uae.1.e executed"
		ai_chance = {
			factor = 90
		}
	}
}

#Puntland declined the offer
country_event = {
	id = uae.2
	title = uae.2.t
	desc = uae.2.d
	picture = GFX_declined_offer_no

	is_triggered_only = yes

	option = {
		name = uae.2.a
		log = "[GetDateText]: [This.GetName]: uae.2.a executed"
		add_opinion_modifier = {
			target = PUN
			modifier = PUN_declined_ships
		}
		add_political_power = -10
		add_equipment_to_stockpile = { type = infantry_weapons amount = 500 }
	}
}
