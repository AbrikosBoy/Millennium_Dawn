##Scripted Localisation for Brazilian GUI
defined_text = {
	name = GetCamaraPresidentName
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_temer_camara_flag
		}
		localization_key = "BRA_ivan_pires"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_eduardo_cunha_flag
		}
		localization_key = "BRA_cunha"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_rodrigo_maia_flag
		}
		localization_key = "BRA_nhonho"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_arthur_lira_flag
		}
		localization_key = "BRA_lira"
	}
}

defined_text = {
	name = GetSenatePresidentName
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_ACM_avo_flag
		}
		localization_key = "BRA_ACM_name"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_davi_alcolumbre_flag
		}
		localization_key = "BRA_alcolumbre"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_rodrigo_pacheco_flag
		}
		localization_key = "BRA_pacheco"
	}
}


defined_text = {
   	name = GetCamaraPresidentPortrait
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_temer_camara_flag
		}
		localization_key = "GFX_BRA_Michel_Temer_Portrait_GUI"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_eduardo_cunha_flag
		}
		localization_key = "GFX_BRA_Eduardo_Cunha"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_rodrigo_maia_flag
		}
		localization_key = "GFX_BRA_Rodrigo_Maia"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_arthur_lira_flag
		}
		localization_key = "GFX_BRA_Arthur_Lira"
	}
}


defined_text = {
   	name = GetSenatePresidentPortrait
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_ACM_avo_flag
		}
		localization_key = "GFX_BRA_ACM_Portrait"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_davi_alcolumbre_flag
		}
		localization_key = "GFX_BRA_Davi_Alcolumbre"
	}
	text = {
		trigger = {
			tag = BRA
			has_country_flag = BRA_rodrigo_pacheco_flag
		}
		localization_key = "GFX_BRA_Rodrigo_Pacheco"
	}
}