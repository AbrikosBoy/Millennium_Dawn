# UAR BALANCE OF POWER MODIFIERS
# AUTHOR(S): DREAD
# THESE ARE PRETTY SIMPLE TO SET UP:
# NAME OF MODIFIER
# AMOUNT OF POWER BALANCE SHIFT
# MINUS IS LEFT AND PLUS IS RIGHT
# DAILY OR WEEKLY

# SECULARISM AND RELIGIOSITY
	ARAB_SECULAR_small_daily = { power_balance_daily = -0.001 }
	ARAB_SECULAR_medium_daily = { power_balance_daily = -0.002 }
	ARAB_SECULAR_large_daily = { power_balance_daily = -0.003 }
	ARAB_SECULAR_small_weekly = { power_balance_weekly = -0.005 }
	ARAB_SECULAR_medium_weekly = { power_balance_weekly = -0.01 }
	ARAB_SECULAR_large_weekly = { power_balance_weekly = -0.015 }
	ARAB_RELIGIOUS_small_daily = { power_balance_daily = 0.001 }
	ARAB_RELIGIOUS_medium_daily = { power_balance_daily = 0.002 }
	ARAB_RELIGIOUS_large_daily = { power_balance_daily = 0.003 }
	ARAB_RELIGIOUS_small_weekly = { power_balance_weekly = 0.005 }
	ARAB_RELIGIOUS_medium_weekly = { power_balance_weekly = 0.01 }
	ARAB_RELIGIOUS_large_weekly = { power_balance_weekly = 0.015 }

# BAATHIST SPLIT
	ARAB_ASSADISM_small_daily = { power_balance_daily = -0.001 }
	ARAB_ASSADISM_medium_daily = { power_balance_daily = -0.002 }
	ARAB_ASSADISM_large_daily = { power_balance_daily = -0.003 }
	ARAB_ASSADISM_small_weekly = { power_balance_weekly = -0.005 }
	ARAB_ASSADISM_medium_weekly = { power_balance_weekly = -0.01 }
	ARAB_ASSADISM_large_weekly = { power_balance_weekly = -0.015 }

	ARAB_SADDAMISM_small_daily = { power_balance_daily = 0.001 }
	ARAB_SADDAMISM_medium_daily = { power_balance_daily = 0.002 }
	ARAB_SADDAMISM_large_daily = { power_balance_daily = 0.003 }
	ARAB_SADDAMISM_small_weekly = { power_balance_weekly = 0.005 }
	ARAB_SADDAMISM_medium_weekly = { power_balance_weekly = 0.01 }
	ARAB_SADDAMISM_large_weekly = { power_balance_weekly = 0.015 }

	ARAB_NASSERISM_PUSH_LEFT_small_daily = { power_balance_daily = -0.001 }
	ARAB_NASSERISM_PUSH_LEFT_medium_daily = { power_balance_daily = -0.002 }
	ARAB_NASSERISM_PUSH_LEFT_large_daily = { power_balance_daily = -0.003 }
	ARAB_NASSERISM_PUSH_LEFT_small_weekly = { power_balance_weekly = -0.005 }
	ARAB_NASSERISM_PUSH_LEFT_medium_weekly = { power_balance_weekly = -0.01 }
	ARAB_NASSERISM_PUSH_LEFT_large_weekly = { power_balance_weekly = -0.015 }
	ARAB_NASSERISM_PUSH_RIGHT_small_daily = { power_balance_daily = 0.001 }
	ARAB_NASSERISM_PUSH_RIGHT_medium_daily = { power_balance_daily = 0.002 }
	ARAB_NASSERISM_PUSH_RIGHT_large_daily = { power_balance_daily = 0.003 }
	ARAB_NASSERISM_PUSH_RIGHT_small_weekly = { power_balance_weekly = 0.005 }
	ARAB_NASSERISM_PUSH_RIGHT_medium_weekly = { power_balance_weekly = 0.01 }
	ARAB_NASSERISM_PUSH_RIGHT_large_weekly = { power_balance_weekly = 0.015 }