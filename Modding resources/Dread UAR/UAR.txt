add_namespace = UAR

country_event = { # [COUNTRY] REQUESTS AID (SYRIAN)
	id = UAR.1

	title = UAR.1.t
	desc = UAR.1.d
	# picture = ADD

	is_triggered_only = yes

	option = { # YES
		name = UAR.1.a
		log = "[GetDateText]: [This.GetName]: UAR.1.a executed"
	}
	option = { # NO
		name = UAR.1.b
		log = "[GetDateText]: [This.GetName]: UAR.1.b executed"
	}
}
country_event = { # [COUNTRY] REQUESTS AID (IRAQI)
	id = UAR.2

	title = UAR.2.t
	desc = UAR.2.d
	# picture = ADD

	is_triggered_only = yes

	option = { # YES
		name = UAR.2.a
		log = "[GetDateText]: [This.GetName]: UAR.2.a executed"
	}
	option = { # NO
		name = UAR.2.b
		log = "[GetDateText]: [This.GetName]: UAR.2.b executed"
	}
}