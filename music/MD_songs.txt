music_station = "MD_Soundtrack"
music = {
	song = "maintheme"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Old Millennium Dawn Main Theme"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Modern Day 4 Main Theme"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
    song = "Brink of War"
    chance = {
        modifier = {
            factor = 1
        }
    }
}

music = {
    song = "24-02-2022"
    chance = {
        modifier = {
            factor = 1
        }
    }
}

music = {
    song = "Soldiers Without Guns"
    chance = {
        modifier = {
            factor = 1
        }
    }
}

music = {
    song = "Technology"
    chance = {
        modifier = {
            factor = 1
        }
    }
}

music = {
	song = "High Seas"
	chance = {
		modifier = {
			factor = 1
		}
	}
}


music = {
	song = "We Remember"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Lost on The Hill"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Memories Without Colour"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Firewall"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Pax Britannica: Uprising"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Pax Britannica: We've Had Enough"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Mopikel - Ambush"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = democratic
			has_war = no
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Mopikel - Back from Hell, alive"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Mopikel - Technologies for Peace"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "Mopikel - Industrial destruction"
	chance = {
		modifier = {
			factor = 1
		}
	}
}

music = {
	song = "War in Ossetia"
	chance = {
		modifier = {
			factor = 1
			tag = GEO
			has_war = yes

		}

		modifier = {
			factor = 0

			OR = {
				NOT = {
					tag = GEO
				}

				has_war = no
			}


		}

	}
}

music = {
	song = "They are coming"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_war = no
		}
	}
}

music = {
	song = "The last one"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_war = no
		}
	}
}

music = {
	song = "Escape from Hell"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_war = no
		}
	}
}

music = {
	song = "Covert ops"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_war = no
		}
	}
}

music = {
	song = "Broken"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_war = no
		}
	}
}

music = {
	song = "War in Kashmir"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_war = no
		}
	}
}

### Fake Commercials
# Set to play seldom - are meant to occationally lift the mood

music = {
	song = "Commercial - Cashkick"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Incredible Opportunity"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Juicy Fresh"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Moon Realestate Corporation"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Nigeria Buisnessman"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - The Cheesy Finale"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - Transnistrian Investment Authority"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commercial - The Love Confirmator"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}