﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_plane1		#C-130 Hercules
		amount = 15
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#F-5
		amount = 87
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1		#F-4 Phantom II
		amount = 95
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter3		#F-16C Fighting Falcon
		amount = 75
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#Mirage F1
		amount = 27
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2		#Mirage 2000
		amount = 34
		producer = FRA
	}
}