﻿division_template = {
	name = "Motoatici Briqadasi (ÖÖA)"		#Motor Rifle Brigade with SPART (after Soviet Motor Rifle Regiment model)

	division_names_group = AZE_MECHANIZED_DIVISIONS

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }

		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
	}

	support = {
		armor_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		Arm_Recce_Comp = { x = 0 y = 2 }
		H_Engi_Comp = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Motoatici Briqadasi (A)"		#Motor Rifle Brigade with ART (after Soviet Motor Rifle Regiment model)

	division_names_group = AZE_MECHANIZED_DIVISIONS

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }

		Arty_Bat = { x = 1 y = 0 }
	}

	support = {
		armor_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
}

units = {
	#1st Corps
	division = {
		name = "1-ci Motoatıcı Briqadası"
		location = 11645		#Evlax
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "3-ci Motoatıcı Briqadası"
		location = 11645		#Evlax
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "9-ci Motoatıcı Briqadası"
		location = 11645		#Evlax
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "10-ci Motoatıcı Briqadası"
		location = 11645		#Evlax
		division_template = "Motoatici Briqadasi (A)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	#2nd Corps
	division = {
		name = "2-ci Motoatıcı Briqadası"
		location = 11704		#Pirekeshkul
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "4-ci Motoatıcı Briqadası"
		location = 11704		#Pirekeshkul
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "6-ci Motoatıcı Briqadası"
		location = 11704		#Pirekeshkul
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "8-ci Motoatıcı Briqadası"
		location = 11704		#Pirekeshkul
		division_template = "Motoatici Briqadasi (A)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	#3rd Corps
	division = {
		name = "7-ci Motoatıcı Briqadası"
		location = 1539		#Shamkir
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "11-ci Motoatıcı Briqadası"
		location = 1539		#Shamkir
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "12-ci Motoatıcı Briqadası"
		location = 1539		#Shamkir
		division_template = "Motoatici Briqadasi (A)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	#4th Corps
	division = {
		name = "41-ci Motoatıcı Briqadası"
		location = 7661		#Baku
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "42-ci Motoatıcı Briqadası"
		location = 7661		#Baku
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "43-ci Motoatıcı Briqadası"
		location = 7661		#Baku
		division_template = "Motoatici Briqadasi (A)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	#5th Corps
	division = {
		name = "51-ci Motoatıcı Briqadası"
		location = 13919		#Mingechevir
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "52-ci Motoatıcı Briqadası"
		location = 13919		#Mingechevir
		division_template = "Motoatici Briqadasi (ÖÖA)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "53-ci Motoatıcı Briqadası"
		location = 13919		#Mingechevir
		division_template = "Motoatici Briqadasi (A)"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72A"
		producer = SOV
		amount = 120
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		producer = SOV
		amount = 103
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BMP-3"
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-2"
		amount = 141
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 110
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 11
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 28
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 44
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 393
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 88
		producer = SOV
		variant_name = "BRDM-2"
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "BM-21 Grad"
		amount = 53
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "2S1 Gvozdika"
		amount = 46
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#D-30 and other guns
		amount = 144
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 80
		producer = SOV
		variant_name = "9K33 Osa"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 54
		producer = SOV
		variant_name = "SA-13 Strela-10"
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1				#Iglas
		amount = 650
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#Fagot
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1			#Konkurs
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#AK-74
		amount = 11000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons2			#AK-74M licensed
		amount = 3500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 1700
		producer = AZE
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 2400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 18
		producer = SOV
		variant_name = "Mil Mi-24"
	}
}