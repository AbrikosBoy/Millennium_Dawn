﻿instant_effect = {
	#Aircraft
	add_equipment_to_stockpile = {
		type = MR_Fighter1			#Chengdu F-7
		amount = 32
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter2			#Mig-29
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter1			#A-5C
		amount = 18
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Mi-17
		amount = 16
		producer = SOV
	}
}