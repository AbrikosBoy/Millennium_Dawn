division_template = {
	name = "Belarusian Volunteers"
	division_names_group = UKR_VOL_01

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		Mot_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Arm_Inf_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		armor_Comp = { x = 0 y = 2 }
	}
}

units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 525
		division_template = "Belarusian Volunteers"
		start_experience_factor = 0.5
		start_equipment_factor = 0.9
		force_equipment_variants = { infantry_weapons2 = { owner = "SOV" } }
		force_equipment_variants = { command_control_equipment = { owner = "SOV" } }
		force_equipment_variants = { Anti_tank_0 = { owner = "SOV" } }
		force_equipment_variants = { Anti_Air_0 = { owner = "SOV" } }
		force_equipment_variants = { APC_2 = { owner = "SOV" } }
		force_equipment_variants = { IFV_2 = { owner = "SOV" } }
		force_equipment_variants = { MBT_2 = { owner = "SOV" } }
	}
}