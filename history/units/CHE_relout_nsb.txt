﻿division_template = {
	name = "Special Purpose battalion"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 1 y = 0 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
}


units = {
	division = {
		name = "Sheikh Mansour Battalion"
		location = 3672		#
		division_template = "Special Purpose battalion"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Dzhokhar Dudayev Battalion"
		location = 3672		#
		division_template = "Special Purpose battalion"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Khamzat Gelayev Battalion"
		location = 683		#
		division_template = "Brigada Natsionalnoi Gvardiy"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1	#
		amount = 9000
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment	#
		amount = 500
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0	#
		amount = 50
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0	#
		amount = 300
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-62"
		amount = 12
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_0	#
		amount = 13
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = artillery_0	#
		amount = 40
		producer = BLR
	}
	add_equipment_to_stockpile = {
		type = APC_1	#
		amount = 50
		producer = BLR
	}

 ########## AIRFORCE
	add_equipment_to_stockpile = { ## L-29s
	type = L_Strike_fighter1
	producer = BLR
	amount = 3
	}
	add_equipment_to_stockpile = { ## Random Belorussian Transports
	type = transport_plane1
	producer = BLR
	amount = 2
	}
}