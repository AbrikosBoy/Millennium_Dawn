﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = cas1		 #SU-25
		amount = 39
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter1		 #Mig-23
		amount = 30
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter1		 #Mig-21
		amount = 60
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter2 	#MiG-29
		amount = 21
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter1 	#Su-22
		#version_name = "Su-22 Fitter"
		amount = 21
		producer = SOV
	}
}