﻿division_template = {
	name = "Batalioni i Komando"
	division_names_group = ALB_SPEC_01
	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	priority = 2
}

units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 9914
		division_template = "Batalioni i Komando"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons 
		amount = 500
		producer = SOV
	}	
}