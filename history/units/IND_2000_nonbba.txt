﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#A4
		amount = 24
		producer = SIN
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#Hawk
		amount = 24
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#F-5
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2		#F-16
		amount = 17
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2		#CN-235 & Similar
		amount = 101
	}
}