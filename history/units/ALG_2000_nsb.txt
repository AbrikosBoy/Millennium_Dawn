﻿division_template = {
	name = "Armoured Division"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		SP_AA_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
	}

	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Mechanised Infantry Division"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		SP_AA_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
	}

	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Airborne Division"

	regiments = {
		Mot_Air_Inf_Bat = { x = 0 y = 0 }
		Mot_Air_Inf_Bat = { x = 0 y = 1 }
		Mot_Air_Inf_Bat = { x = 1 y = 0 }
		Mot_Air_Inf_Bat = { x = 1 y = 1 }
		Special_Forces = { x = 2 y = 0 }
	}
}

division_template = {
	name = "Republican Guard Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Armoured Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		SP_AA_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Mechanized Infantry Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Independent Infantry Battalion"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
	}
}

units = {
	#Only 1st, 8th, 12th and 40th division have public names
	division = {
		name = "8th Armoured Division"
		location = 7065		#Ras El Ma
		division_template = "Armoured Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "1st Armoured Division"
		location = 7132
		division_template = "Armoured Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "40th Mechanised Infantry Division"
		location = 10731	#Near Moroccan border
		division_template = "Mechanised Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "12th Mechanised Infantry Division"
		location = 5035
		division_template = "Mechanised Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	#Disclosed location and name for these units
	division = {
		name = "17th Airborne Division"
		location = 5095
		division_template = "Airborne Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	##Republican Guard
	division = {
		name = "Republican Guard Brigade"
		location = 1145
		division_template = "Republican Guard Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	##Independent Armoured Brigade
	division = {
		name = "30th Armoured Brigade"
		location = 4116
		division_template = "Armoured Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	##Independent Mechanized Infantry Brigades
	division = {
		name = "29th Mechanized Infantry Brigade"
		location = 1160
		division_template = "Mechanized Infantry Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "91st Mechanized Infantry Brigade"
		location = 8077
		division_template = "Mechanized Infantry Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "56th Mechanized Infantry Brigade"
		location = 7166
		division_template = "Mechanized Infantry Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "78th Mechanized Infantry Brigade"
		location = 9960
		division_template = "Mechanized Infantry Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	##Independent Infantry Battalions (Oh God, why?!)
	division = {
		name = "11th Infantry Battalion"
		location = 1160
		division_template = "Independent Infantry Battalion"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "212th Infantry Battalion"
		location = 11953
		division_template = "Independent Infantry Battalion"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "22nd Infantry Battalion"
		location = 1058
		division_template = "Independent Infantry Battalion"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "184th Infantry Battalion"
		location = 12034
		division_template = "Independent Infantry Battalion"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		producer = SOV
		variant_name = "T-72A"
		amount = 350
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		producer = SOV
		variant_name = "T-62M" # no reference for t62A, so used a good upgrade
		amount = 330
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		producer = SOV
		variant_name = "T-55"
		amount = 320
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-2"
		amount = 289
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 700
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 200
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-50"
		amount = 30
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "FAHD"
		amount = 100
		producer = EGY
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M3 Panhard"
		amount = 55
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 124
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 44
		producer = FRA
		variant_name = "Panhard AML 60"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "OT-64"
		amount = 150
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "2S3 Akatsiya"
		amount = 35
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "2S1 Gvozdika"
		amount = 150
	}

	add_equipment_to_stockpile = {
		type = artillery_0	#D-30
		amount = 198
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "BM-30 Smerch"
		amount = 18
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "BM-21 Grad"
		amount = 30
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		variant_name = "ZSU-23-4 Shilka"
		amount = 310
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 44
		producer = SOV
		variant_name = "9K33 Osa"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 44
		producer = SOV
		variant_name = "SA-6 Gainful"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 20
		producer = SOV
		variant_name = "SA-9 Gaskin"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 32
		producer = SOV
		variant_name = "SA-13 Strela-10"
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#MILANs
		amount = 50
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_2			#Metis
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0			#Malyutka
		amount = 250
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 300
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#Fagot
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1			#Konkurs
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0			#Strela
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#Licensed AK-74s/Type 56
		amount = 12000
		producer = ALG
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0			#
		amount = 1200
		producer = ALG
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		variant_name = "Mil Mi-24"
		amount = 31
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-8/17/171
		amount = 76
		producer = SOV
	}

	#To fill up what's missing
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 1200
		producer = ALG
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 400
		producer = ALG
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 150
		producer = ALG
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0
		amount = 200
		producer = ALG
	}
}