﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aero L-39"
		type = small_plane_strike_airframe_1
		amount = 2
		producer = CZE
	}
}