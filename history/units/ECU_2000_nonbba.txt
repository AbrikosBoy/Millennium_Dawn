instant_effect = {

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		amount = 8
		#variant_name = "SEPECAT Jaguar"
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		amount = 10
		#variant_name = "IAI Kfir"
		producer = ISR
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		amount = 20
		#variant_name = "A-37 Dragonfly"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1		#in place of T-34, T-41
		amount = 18
		#variant_name = "T-38C Talon"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		amount = 13
		#variant_name = "Mirage F1"
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1	#added extra to account for Boeing Transports
		amount = 11
		#variant_name = "C-130 Hercules"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		amount = 3
		#variant_name = "DHC-4 Caribou"
		producer = CAN
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		amount = 3
		#variant_name = "CASA/IPTN CN-235"
		producer = SPR
	}

}