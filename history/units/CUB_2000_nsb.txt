﻿division_template = {
	name = "Tank Division"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 2 }

		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		armor_Bat = { x = 1 y = 3 }

		SP_Arty_Bat = { x = 2 y = 0 }
		SP_AA_Bat = { x = 2 y = 1 }

	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Mechanized Infantry Division"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }

		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Arm_Inf_Bat = { x = 1 y = 2 }

		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 2 }

		SP_Arty_Bat = { x = 3 y = 0 }
		SP_AA_Bat = { x = 3 y = 1 }

	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Infantry Division"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }

		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }

		L_Inf_Bat = { x = 2 y = 0 }

		Arty_Bat = { x = 2 y = 0 }

	}
}
division_template = {
	name = "Spetsnaz Brigade"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		Special_Forces = { x = 0 y = 3 }
	}
	priority = 2
}
units = {
	division = {
		name = "3 División Blindada"
		location = 7622	#
		division_template = "Tank Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.02
	}
	division = {
		name = "70 División Mecanizada"
		location = 12347#
		division_template = "Mechanized Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "50 División Mecanizada"
		location = 9603	#
		division_template = "Mechanized Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "31 División de Infantería"
		location = 7622	#
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	division = {
		name = "81 División de Infantería"
		location = 9613	#
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	division = {
		name = "41 División de Infantería"
		location = 7451	#
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	division = {
		name = "43 División de Infantería"
		location = 10374	#
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	division = {
		name = "4895 La Brigada Móvil de Tropas Especiales de Cuba"
		location = 4476	#
		division_template = "Spetsnaz Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}


instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons		#
		amount = 14500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment		#
		amount = 1800
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 600
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-62"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 20
		producer = SOV
		variant_name = "PT-76"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 700
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0	#D-30
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S3 Akatsiya"
		amount = 20
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 20
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0			#Malyutka
		amount = 600
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#Fagot
		amount = 600
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0
		amount = 300
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0			#SA-7
		amount = 250
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1			#SA-16 Gimlet
		amount = 250
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 45
		producer = SOV
		variant_name = "ZSU-23-4 Shilka"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0			#SA-8 Osa
		amount = 15
		producer = SOV
		variant_name = "9K33 Osa"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 20
		producer = SOV
		variant_name = "SA-9 Gaskin"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 10
		producer = SOV
		variant_name = "SA-13 Strela-10"
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 600
		producer = SOV
	}
}