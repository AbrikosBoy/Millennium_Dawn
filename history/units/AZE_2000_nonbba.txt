instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#MiG-21
		amount = 4
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Su-24
		amount = 5
				producer = SOV
	}

	add_equipment_to_stockpile = {
		type = cas1 #Su-25 Frogfoot-A
		amount = 2
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter1	#MiG-25
		amount = 34
		producer = SOV
		#version_name = "MiG-25 Foxbat"
		}

	add_equipment_to_stockpile = {
		type = Strike_fighter1	#Su-22
		amount = 4
		producer = SOV
	}
}