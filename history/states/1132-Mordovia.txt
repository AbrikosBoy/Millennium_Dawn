state = {
	id = 1132
	name = "STATE_1132"
	
	resources = {
		oil = 15
	}
	
	history = {
		owner = SOV
		victory_points = {
			324 5
		}
		buildings = {
			infrastructure = 2
			internet_station = 1
			industrial_complex = 1
		}
		add_core_of = SOV
		add_core_of = MOV
	}

	provinces = {
	   324 3197 6294 6329 6343 9276 11262 
	}
	manpower = 703287
	buildings_max_level_factor = 1.000
	state_category = state_02
}
