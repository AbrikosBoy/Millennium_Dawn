# Issue Name

_Ensure the issue has the labels ``Enhancement`` and ``Discussion``. Furthermore the words ``System:`` or ``Enhancement:`` should be placed in the title of this issue. Similar to focus trees the milestone should also be assigned for the projected release date. Ensure you also provide the following information:_

Project Name: ``Replace me with a project name``

Branch: ``Branch where the work will take place``

## Purpose

_Replace me with a short description about what the purpose of this issue is. It can be short and concise._

## Importance

_1 to 10 -- 10 being the most important, 1 being the least important. Scaling revolves around the following guidelines: 1-5; Minor Mechanics, 5-8; Country Specific Content, 8-10; Global Mechanic or Changes_

## Description of the Issue

_This section is where you put the "meat" of the issue. Any details, images or otherwise you would like to attach to the issue place below._

/milestone %"Version 1.9 Roadmap"

/assign me

/label ~Enhancement