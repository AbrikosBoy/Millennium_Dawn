# Summary

_DELETE ME: Replace me with details of the merge request. Specifically, state all of the changes that are being made within the requested merged branch._
_DELETE ME: When creating your merge request ensure you enable the buttons "squash commits" and "delete source branch". This is for cleanup purposes when a project is now merged into master. Branches should be short lived and merged quickly so they do not run into "age" issues. If your project is still ongoing we want to delete the branch and then recreate it from master to "clean" the git history. If you have questions please ping @AngriestBird._

_DELETE ME: Please make sure you have updated the changelog with any of your changes that are to be merged._

Example:

- Added new tags TAG TAG TAG TAG
- New focus tree for Slavistan
- Added new game rules for Monaco
- New python test

## Importance

_DELETE ME: 1 to 10 -- 10 being the most important, 1 being the least important. Scaling revolves around the following guidelines: 1-5; Minor Mechanics, 5-8; Country Specific Content, 8-10; Global Mechanic or Changes_

## Related Issues

_DELETE ME: In this section you need to replace the below #num with the number of your issue. This assigns this merge request to a selected issue since we do not create branches off the issue._

_DELETE ME: Remove this if you don't have a gitlab issue_
Closes #num

/milestone %"Version 1.9 Roadmap"

/assign me

/reviewer @AngriestBird @WarnerDev @Kalkalash @Fire_Hair @amtoj @crocomoth @alexmarkel0v @TraianoGitHub @KianGHK1530 @Razmode @wellihavenoidea @Patrador @heastel
