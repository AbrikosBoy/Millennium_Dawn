BUL_support_bosnian_against_rsk = {
	allowed = { original_tag = BUL }
	enable = {
		BOS = {
			has_country_flag = defeat_all_serbs
			OR = {
				has_idea = BOS_EUFOR
				has_idea = BOS_SFOR
			}
		}
		RSK = { has_war_with = BOS }
		has_idea = bos_sfor_vol_troops
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = send_volunteers_desire id = "BOS" value = 200 }
	ai_strategy = { type = befriend id = "BOS" value = 200 }
	ai_strategy = { type = support id = "BOS" value = 200 }
}

BUL_alliance_with_csto = {
	allowed = { original_tag = BUL }
	enable = {
		has_completed_focus = BUL_align_to_csto
		SOV = { has_idea = CSTO_member }

	}
	abort_when_not_enabled = yes

	ai_strategy = { type = alliance id = "SOV" value = 100 }
	ai_strategy = { type = befriend id = "SOV" value = 100 }
	ai_strategy = { type = support id = "SOV" value = 100 }
}
