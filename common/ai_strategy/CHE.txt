CHE_friend_russia = {
	allowed = { original_tag = CHE }

	enable = {
		is_subject_of = SOV
		country_exists = SOV
		has_completed_focus = CHE_strengthen_ties_moscow
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SOV" value = 200 }
}
CHE_friend_palestine = {
	allowed = { original_tag = CHE }

	enable = {
		is_subject_of = SOV
		country_exists = PAL
		has_completed_focus = CHE_palestine
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "PAL" value = 200 }
	ai_strategy = { type = send_volunteers_desire id = "PAL" value = 1000 }
	ai_strategy = { type = antagonize id = "ISR" value = 200 }
}
CHE_narcos_in_rus = {
	allowed = { original_tag = SOV }

	enable = {
		CHE = {
			NOT = { is_subject_of = SOV }
			has_completed_focus = CHE_secret_export_to_russia
		}
		NOT = { is_in_faction_with = CHE }
		country_exists = CHE
		
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "CHE" value = 100 }
	ai_strategy = { type = conquer id = "CHE" value = 100 }
}
CHE_raids_in_rus = {
	allowed = { original_tag = SOV }

	enable = {
		CHE = {
			NOT = { is_subject_of = SOV }
			has_completed_focus = CHE_ichkerian_brotherhood
		}
		NOT = { is_in_faction_with = CHE }
		country_exists = CHE
		
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "CHE" value = 1000 }
	ai_strategy = { type = conquer id = "CHE" value = 1000 }
}
CHE_raids_in_rus_salofit = {
	allowed = { original_tag = SOV }

	enable = {
		CHE = {
			NOT = { is_subject_of = SOV }
			has_completed_focus = CHE_start_raids_into_russia
			salafist_caliphate_are_in_power = yes
		}
		NOT = { is_in_faction_with = CHE }
		country_exists = CHE
		
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "CHE" value = 1000 }
	ai_strategy = { type = conquer id = "CHE" value = 1000 }
}
CHE_turkey_brazzers = {
	allowed = { original_tag = TUR }

	enable = {
		CHE = {
			NOT = { is_subject_of = SOV }
			nationalist_military_junta_are_in_power = yes
			has_completed_focus = CHE_turkish_azeri_raltions
		}
		country_exists = CHE
		
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "CHE" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "CHE" value = 100 }
}
CHE_aliev_brazzers = {
	allowed = { original_tag = AZE }

	enable = {
		CHE = {
			NOT = { is_subject_of = SOV }
			nationalist_military_junta_are_in_power = yes
			has_completed_focus = CHE_turkish_azeri_raltions
		}
		country_exists = CHE
		
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "CHE" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "CHE" value = 100 }
}
CHE_influence_UKR = {
	allowed = { original_tag = CHE }
	enable = {
		is_subject_of = SOV
		NOT = { is_in_faction_with = UKR }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = contain id = "UKR" value = 50 }
	ai_strategy = { type = antagonize id = "UKR" value = 25 }
	ai_strategy = { type = befriend id = "DPR" value = 100 }
	ai_strategy = { type = support id = "DPR" value = 500 }
	ai_strategy = { type = send_volunteers_desire id = "DPR" value = 1000 }

}
CHE_SOV_owns_crimea = {
	allowed = { original_tag = CHE }
	enable = {
		is_subject_of = SOV
		SOV = {
			OR = {
				owns_state = 669
				controls_state = 669
			}
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = front_unit_request tag = UKR value = 200 }
	ai_strategy = { type = force_build_armies value = 150 }
}
CHE_factory_target = {
	allowed = { original_tag = CHE }
	enable = { always = yes }
	abort_when_not_enabled = yes

	ai_strategy = { type = building_target id = industrial_complex value = 100 }
}
