# Written by Andreas "Cold Evul" Brostrom
PMR_support_ABK = {
	allowed = { original_tag = PMR }
	enable = {
		country_exists = ABK
	}
	abort = {
		has_war_with = ABK
	}
	ai_strategy = { type = befriend id = "ABK" value = 25 }
}

PMR_hates_moldova = {
	allowed = { original_tag = PMR }
	enable = {
		country_exists = MLV #Moldova
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = contain id = "MLV" value = 50 }
	ai_strategy = { type = antagonize id = "MLV" value = 50 }
}
SOV_support_PMR = {
	allowed = { original_tag = SOV }
	enable = {
		PMR = { has_completed_focus = PMR_ruble }
		country_exists = PMR
	}
	abort = {
		has_war_with = PMR
	}
	ai_strategy = { type = befriend id = "PMR" value = 50 }
	ai_strategy = { type = support id = "PMR" value = 50 }
}
SOV_support_PMR2 = {
	allowed = { original_tag = SOV }
	enable = {
		PMR = { has_completed_focus = PMR_citizenship }
		country_exists = PMR
	}
	abort = {
		has_war_with = PMR
	}
	ai_strategy = { type = befriend id = "PMR" value = 50 }
	ai_strategy = { type = support id = "PMR" value = 50 }
}
SOV_support_PMR3 = {
	allowed = { original_tag = SOV }
	enable = {
		PMR = { has_completed_focus = PMR_russian_flag }
		country_exists = PMR
	}
	abort = {
		has_war_with = PMR
	}
	ai_strategy = { type = befriend id = "PMR" value = 50 }
	ai_strategy = { type = support id = "PMR" value = 50 }
}
SOV_support_pmr_14_army = {
	allowed = { original_tag = SOV }
	enable = {
		PMR = {
			has_idea = PMR_14_army
			has_war = yes
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = send_volunteers_desire
		id = PMR
		value = 1500
	}
}
ABK_support_PMR = {
	allowed = { original_tag = ABK }
	enable = {
		PMR = { has_completed_focus = PMR_cis_2 }
		country_exists = PMR
	}
	abort = {
		has_war_with = PMR
	}
	ai_strategy = { type = befriend id = "PMR" value = 50 }
	ai_strategy = { type = support id = "PMR" value = 50 }
}
SOO_support_PMR = {
	allowed = { original_tag = SOO }
	enable = {
		PMR = { has_completed_focus = PMR_cis_2 }
		country_exists = PMR
	}
	abort = {
		has_war_with = PMR
	}
	ai_strategy = { type = befriend id = "PMR" value = 50 }
	ai_strategy = { type = support id = "PMR" value = 50 }
}
NKR_support_PMR = {
	allowed = { original_tag = NKR }
	enable = {
		PMR = { has_completed_focus = PMR_cis_2 }
		country_exists = PMR
	}
	abort = {
		has_war_with = PMR
	}
	ai_strategy = { type = befriend id = "PMR" value = 50 }
	ai_strategy = { type = support id = "PMR" value = 50 }
}
PMR_dont_declare_on_romania = {
	allowed = { original_tag = PMR }
	enable = {
		original_tag = PMR
		has_wargoal_against = ROM
		strength_ratio = {
			tag = ROM
			ratio < 1.0
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = conquer id = "ROM" value = -300 }
	ai_strategy = { type = declare_war id = "ROM" value = -300 }
}
PMR_dont_declare_on_ukraine = {
	allowed = { original_tag = PMR }
	enable = {
		original_tag = PMR
		has_wargoal_against = UKR
		strength_ratio = {
			tag = UKR
			ratio < 1.0
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = conquer id = "UKR" value = -300 }
	ai_strategy = { type = declare_war id = "UKR" value = -300 }
}
PMR_influence_ukr = {
	allowed = { original_tag = PMR }
	enable = {
		PMR = { has_completed_focus = PMR_lebedev }
		NOT = { UKR = {	influence_higher_30 = yes } }
		country_exists = UKR
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = influence id = "UKR" value = 1000 }
}
PMR_influence_mlv = {
	allowed = { original_tag = PMR }
	enable = {
		PMR = { has_completed_focus = PMR_lebedev }
		NOT = { MLV = {	influence_higher_30 = yes } }
		country_exists = MLV
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = influence id = "MLV" value = 1000 }
}