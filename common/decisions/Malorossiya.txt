MLR_manifest_decisions = {

	MLR_kirov_core = {
		cost = 30
		days_remove = 70
		icon = GFX_decision_moa_button
		fire_only_once = yes
		visible = {
			has_completed_focus = MLR_start
		}

		remove_effect = {
			1090 = { add_core_of = MLR }
		}
	}
	MLR_kiev_core = {
		cost = 30
		days_remove = 70
		icon = GFX_decision_moa_button
		fire_only_once = yes
		visible = {
			has_completed_focus = MLR_start
		}

		remove_effect = {
			698 = { add_core_of = MLR }
		}
	}
	MLR_poltava_core = {
		cost = 30
		days_remove = 70
		icon = GFX_decision_moa_button
		fire_only_once = yes
		visible = {
			has_completed_focus = MLR_start
		}

		remove_effect = {
			1089 = { add_core_of = MLR }
		}
	}
}

