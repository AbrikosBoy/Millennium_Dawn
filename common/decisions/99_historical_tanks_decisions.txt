historical_tanks_category = {
	# Russian stuff
	t90_m_decision = {
		icon = GFX_decision_t90_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = medium_gun_tech_3
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = era_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t90_m_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "T-90M"
				type = mbt_hull_2
				modules = {
					main_armament_slot = tank_medium_cannon_3
					turret_type_slot = tank_soviet_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = smoothbore_atgm_gen3
					special_type_slot_4 = tank_battlestation_3
					special_type_slot_5 = reactive_armor_gen3
				}
				upgrades = {
					tank_nsb_armor_upgrade = 2
				}
				icon = "GFX_SOV_MBT_4"
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	t72b3_decision = {
		icon = GFX_decision_t90_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = medium_gun_tech_2
			has_tech = diesel_engine_tech_4
			has_tech = tank_components_tech_3
			has_tech = era_tech_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t72b3_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "T-72B3"
				type = mbt_hull_1
				modules = {
					main_armament_slot = tank_medium_cannon_2
					turret_type_slot = tank_soviet_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_diesel_engine_gen4
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = smoothbore_atgm_gen2
					special_type_slot_4 = tank_battlestation_3
					special_type_slot_5 = reactive_armor_gen2
				}
				upgrades = {
					tank_nsb_armor_upgrade = 3
				}
				obsolete = yes
				icon = "GFX_SOV_MBT_9_medium"
				model = "SOV_T72A_entity"
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	t72b3_m2016_decision = {
		icon = GFX_decision_t90_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = medium_gun_tech_3
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = era_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t72b3_m2016_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "T-72B3m2016"
				type = mbt_hull_1
				modules = {
					main_armament_slot = tank_medium_cannon_3
					turret_type_slot = tank_soviet_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = smoothbore_atgm_gen2
					special_type_slot_4 = tank_battlestation_3
					special_type_slot_5 = reactive_armor_gen3
				}
				upgrades = {
					tank_nsb_armor_upgrade = 3
				}
				obsolete = yes
				icon = "GFX_SOV_MBT_9_medium"
				model = "SOV_T72A_entity"
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	t80bvm_decision = {
		icon = GFX_decision_t90_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = medium_gun_tech_3
			has_tech = turbine_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = era_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t80bvm_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "T-80BVM"
				type = mbt_hull_1
				modules = {
					main_armament_slot = tank_medium_cannon_3
					turret_type_slot = tank_soviet_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_gas_turbine_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = smoothbore_atgm_gen3
					special_type_slot_4 = tank_battlestation_3
					special_type_slot_5 = reactive_armor_gen3
				}
				upgrades = {
					tank_nsb_armor_upgrade = 2
				}
				icon = "GFX_SOV_MBT_4"
			}
		}

		ai_will_do = {
			factor = 10
		}
	}


	t14_decision = {
		icon = GFX_decision_armata_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = medium_gun_tech_3
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_4
			has_tech = era_tech_3
			has_tech = mbt_tech_3
			has_tech = armor_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t14_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "T-14 Armata"
				type = mbt_hull_3
				modules = {
					main_armament_slot = tank_medium_cannon_3
					turret_type_slot = tank_unmanned_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen3
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = hardkill_system_gen1
					special_type_slot_4 = tank_battlestation_4
					special_type_slot_5 = reactive_armor_gen3
				}
				upgrades = {
					tank_nsb_armor_upgrade = 2
				}
				icon = "GFX_SOV_MBT_5"
			}
		}

		ai_will_do = { factor = 0 }
	}

	t14_152_decision = {
		icon = GFX_decision_armata_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = very_large_gun_tech
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_4
			has_tech = era_tech_3
			has_tech = mbt_tech_3
			has_tech = armor_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t14_152_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "T-14/152 Armata"
				type = mbt_hull_3
				modules = {
					main_armament_slot = tank_very_large_cannon
					turret_type_slot = tank_unmanned_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen3
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = hardkill_system_gen1
					special_type_slot_4 = tank_battlestation_4
					special_type_slot_5 = reactive_armor_gen3
				}
				upgrades = {
					tank_nsb_armor_upgrade = 2
				}
				icon = "GFX_SOV_MBT_5"
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	t15_decision = {
		icon = GFX_decision_t15_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = big_autocannon_tech_3
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = afv_tech_3
			has_tech = armor_tech_3
			has_tech = Heavy_Anti_tank_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision t15_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "T-15"
				type = ifv_hull_3
				modules = {
					main_armament_slot = big_autocannon_3
					turret_type_slot = afv_unmanned_turret
					suspension_type_slot = afv_torsion_bar_suspension
					armor_type_slot = afv_composite_armor_gen3
					engine_type_slot = tank_diesel_engine_gen5
					special_armament_type_slot = afv_coax_machine_gun_3
					special_type_slot_1 = afv_atgm_gen4
					special_type_slot_2 = smoke_launchers_2
					special_type_slot_3 = afv_mine_protection
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 4
				}
				icon = "GFX_ifv_hull_3_a"
			}
		}

		ai_will_do = { factor = 0 }
	}

	kurganets_decision = {
		icon = GFX_decision_kurganets_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = infantry_weapons4
			has_tech = diesel_engine_tech_4
			has_tech = afv_tech_3
			has_tech = Heavy_Anti_tank_3
			has_tech = tank_components_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision kurganets_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "Kurganets-25"
				type = apc_hull_3
				modules = {
					main_armament_slot = afv_machine_gun_3
					turret_type_slot = afv_unmanned_turret
					suspension_type_slot = afv_torsion_bar_suspension
					armor_type_slot = afv_alum_armor_gen3
					engine_type_slot = tank_diesel_engine_gen4
					special_type_slot_replacement = afv_additional_machine_guns
					special_type_slot_1 = afv_gun_ports
					special_type_slot_2 = afv_mine_protection
					special_type_slot_3 = afv_atgm_gen4
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 3
				}
				icon = "GFX_apc_hull_3_a"
			}
		}

		ai_will_do = { factor = 0 }
	}

	msta_decision = {
		icon = GFX_decision_msta_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = tank_components_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision msta_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "2S33 Msta-SM2"
				type = spart_hull_2
				modules = {
					arty_main_armament_slot = art_med_gun_gen2
					chassis_type_slot = chassis_tank_gen1
					engine_type_slot = tank_diesel_engine_gen3
					conversion_type_slot = empty
					special_type_slot_1 = support_ammo_medium_gen2
					special_type_slot_2 = heat_ammo_gun_medium_gen2
					special_type_slot_3 = rocket_assisted_ammo_gun_medium_gen2
					special_type_slot_4 = art_battlestation_gen3
				}
				upgrades = {
					art_nsb_fire_upgrade = 3
				}
				icon = "GFX_spart_hull_2_t"
			}
		}

		ai_will_do = { factor = 10 }
	}

	koalitsiya_decision = {
		icon = GFX_decision_koalitsiya_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = tank_components_tech_3
			has_tech = nsb_artillery_2
			has_tech = nsb_SP_arty_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision koalitsiya_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "2S35 Koalitsiya-SV"
				type = spart_hull_3
				modules = {
					arty_main_armament_slot = art_med_gun_gen3
					chassis_type_slot = chassis_tank_gen2
					engine_type_slot = tank_diesel_engine_gen4
					conversion_type_slot = empty
					special_type_slot_1 = support_ammo_medium_gen3
					special_type_slot_2 = heat_ammo_gun_medium_gen2
					special_type_slot_3 = rocket_assisted_ammo_gun_medium_gen2
					special_type_slot_4 = art_battlestation_gen3
				}
				upgrades = {
					art_nsb_fire_upgrade = 3
				}
				icon = "GFX_spart_hull_2_t"
			}
		}

		ai_will_do = { factor = 0 }
	}

	sprut_decision = {
		icon = GFX_decision_sprut_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = light_tank_tech_2
			has_tech = diesel_engine_tech_4
			has_tech = tank_components_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision sprut_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "2S25 Sprut-SD"
				type = light_tank_hull_2
				parent_version = 0
				modules = {
					main_armament_slot = tank_small_medium_cannon_2
					turret_type_slot = tank_base_tank_turret
					chassis_type_slot = chassis_tracked_afv_gen2
					engine_type_slot = tank_diesel_engine_gen4
					armor_type_slot = afv_alum_armor_gen2
					reload_type_slot = automatic_loading
					special_armament_type_slot = additional_machine_guns
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = empty
					special_type_slot_3 = empty
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 1
				}
				icon = "GFX_SOV_Rec_tank_2_medium"
			}
		}

		ai_will_do = { factor = 0 }
	}

	terminator_decision = {
		icon = GFX_decision_termonator_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = SOV
		}

		available = {
			has_tech = light_tank_tech_2
			has_tech = diesel_engine_tech_4
			has_tech = tank_components_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision sprut_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "BMPT Terminator"
				type = light_tank_hull_2
				parent_version = 0
				modules = {
					main_armament_slot = apc_small_autocannon_2
					turret_type_slot = afv_no_turret
					chassis_type_slot = chassis_light_tank_gen1
					engine_type_slot = tank_diesel_engine_gen4
					armor_type_slot = afv_composite_armor_gen2
					reload_type_slot = light_tank_afv_automatic_loading
					special_armament_type_slot = empty
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = reactive_armor_gen2
					special_type_slot_3 = empty
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 2
				}
				icon = "GFX_SOV_Rec_tank_2_medium"
			}
		}

		ai_will_do = { factor = 10 }
	}

	# USA
	abrams_decision = {
		icon = GFX_decision_abrams_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = USA
		}

		available = {
			has_tech = turbine_engine_tech_5
			has_tech = tank_components_tech_4
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision abrams_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "M1A2 SepV3 Abrams"
				type = mbt_hull_1
				modules = {
					main_armament_slot = tank_medium_cannon_2
					turret_type_slot = tank_base_tank_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_gas_turbine_engine_gen5
					reload_type_slot = manual_loading
					special_type_slot_1 = additional_machine_guns
					special_type_slot_2 = smoke_launchers_2
					special_type_slot_3 = hardkill_system_gen1
					special_type_slot_4 = tank_battlestation_4
					special_type_slot_5 = reactive_armor_gen2
				}
				upgrades = {
					tank_nsb_armor_upgrade = 5
				}
				icon = "GFX_USA_MBT_3"
			}
		}

		ai_will_do = { factor = 10 }
	}

	bradley_decision = {
		icon = GFX_decision_bradley_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = USA
		}

		available = {
			has_tech = tank_components_tech_3
			has_tech = afv_tech_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision bradley_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "M2A3 Bradley"
				type = ifv_hull_2
				parent_version = 1
				modules = {
					main_armament_slot = small_autocannon_2
					turret_type_slot = afv_base_turret
					suspension_type_slot = afv_torsion_bar_suspension
					armor_type_slot = afv_steel_armor_gen2
					engine_type_slot = tank_diesel_engine_gen2
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = afv_atgm_gen3
					special_type_slot_3 = afv_reverse_doors
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 2
				}
				icon = "GFX_USA_IFV_4"
				model = "USA_M2A3bradley_entity"
				design_team = mio:USA_united_defense_tank_manufacturer
			}
		}

		ai_will_do = { factor = 10 }
	}

	booker_decision = {
		icon = GFX_decision_m10_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = USA
		}

		available = {
			has_tech = light_tank_tech_3
			has_tech = diesel_engine_tech_4
			has_tech = tank_components_tech_3
			has_tech = small_medium_gun_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision booker_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "M10 Booker"
				type = light_tank_hull_3
				modules = {
					main_armament_slot = tank_small_medium_cannon_3
					turret_type_slot = tank_base_tank_turret
					chassis_type_slot = chassis_light_tank_gen2
					engine_type_slot = tank_diesel_engine_gen4
					armor_type_slot = afv_composite_armor_gen2
					reload_type_slot = manual_loading
					special_armament_type_slot = additional_machine_guns
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = empty
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 3
				}
				icon = "GFX_USA_Rec_tank_4_medium"
			}
		}

		ai_will_do = { factor = 10 }
	}

	new_paladin_decision = {
		icon = GFX_decision_paladin_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = USA
		}

		available = {
			has_tech = tank_components_tech_3
			has_tech = nsb_SP_arty_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision new_paladin_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "M109A6 Paladin"
				type = spart_hull_2
				modules = {
					arty_main_armament_slot = art_med_gun_gen2
					chassis_type_slot = chassis_afv_gen2
					engine_type_slot = tank_diesel_engine_gen3
					conversion_type_slot = empty
					special_type_slot_1 = support_ammo_medium_gen2
					special_type_slot_2 = cluster_ammo_gun_medium_gen2
					special_type_slot_3 = laser_ammo_gun_medium_gen2
					special_type_slot_4 = art_battlestation_gen3
				}
				upgrades = {
					art_nsb_fire_upgrade = 3
				}
				icon = "GFX_USA_spart_hull_1_a"
			}
		}

		ai_will_do = { factor = 0 }
	}

	m1128_decision = {
		icon = GFX_decision_stryker_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = USA
		}

		available = {
			has_tech = tank_components_tech_2
			has_tech = afv_tech_2
			has_tech = small_medium_gun_tech_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision m1128_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "M1128"
				type = ifv_hull_1
				modules = {
					main_armament_slot = afv_small_medium_cannon_2
					turret_type_slot = afv_base_turret
					suspension_type_slot = afv_tracked_suspension
					armor_type_slot = afv_steel_armor_gen2
					engine_type_slot = tank_diesel_engine_gen3
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = empty
					special_type_slot_3 = afv_additional_machine_guns
					special_type_slot_4 = afv_battlestation_2
					special_slot_type_5 = empty
				}
				upgrades = {
					afv_nsb_armor_upgrade = 2
				}
				icon = "GFX_USA_IFV_2"
			}
		}

		ai_will_do = { factor = 0 }
	}

	# CHI
	type_99_decision = {
		icon = GFX_decision_type99_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = CHI
		}

		available = {
			has_tech = medium_gun_tech_3
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = era_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision type_99_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "Type-99A"
				type = mbt_hull_2
				parent_version = 0
				modules = {
					main_armament_slot = tank_medium_cannon_2
					turret_type_slot = tank_soviet_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = additional_machine_guns
					special_type_slot_2 = smoke_launchers_2
					special_type_slot_4 = tank_battlestation_3
					special_type_slot_5 = reactive_armor_gen2
				}
				upgrades = {
					tank_nsb_armor_upgrade = 4
				}
				icon = "GFX_CHI_MBT_4"
			}
		}

		ai_will_do = { factor = 0 }
	}

	plz_05_decision = {
		icon = GFX_decision_plz_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = CHI
		}

		available = {
			has_tech = tank_components_tech_3
			has_tech = nsb_SP_arty_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision plz_05_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "PLZ-05"
				type = spart_hull_2
				modules = {
					arty_main_armament_slot = art_med_gun_gen2
					chassis_type_slot = chassis_afv_gen2
					engine_type_slot = tank_diesel_engine_gen2
					conversion_type_slot = empty
					special_type_slot_1 = support_ammo_medium_gen2
					special_type_slot_2 = laser_ammo_gun_medium_gen2
					special_type_slot_3 = cluster_ammo_gun_medium_gen2
					special_type_slot_4 = art_battlestation_gen3
				}
				upgrades = {
					art_nsb_fire_upgrade = 3
				}
				icon = "GFX_spart_hull_2_t"
			}
		}

		ai_will_do = { factor = 0 }
	}

#	# GER
#	## Leopards and stuff are in german content itself
#	lynx_decision = {
#		icon = GFX_decision_generic_tank
#
#		fire_only_once = yes
#		custom_cost_trigger = {
#			has_army_experience > 34
#		}
#		custom_cost_text = army_xp_35
#
#		visible = {
#			original_tag = GER
#		}
#
#		available = {
#			has_tech = big_autocannon_tech_3
#			has_tech = diesel_engine_tech_5
#			has_tech = tank_components_tech_3
#			has_tech = afv_tech_3
#			has_tech = armor_tech_3
#			has_tech = Heavy_Anti_tank_3
#		}
#
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision lynx_decision"
#			army_experience = -35
#			create_equipment_variant = {
#				name = "Lynx"
#				type = ifv_hull_3
#				modules = {
#					main_armament_slot = big_autocannon_3
#					turret_type_slot = afv_unmanned_turret
#					suspension_type_slot = afv_torsion_bar_suspension
#					armor_type_slot = afv_composite_armor_gen3
#					engine_type_slot = tank_diesel_engine_gen5
#					special_armament_type_slot = double_feed_system
#					special_type_slot_1 = afv_atgm_gen4
#					special_type_slot_2 = smoke_launchers_2
#					special_type_slot_3 = afv_mine_protection
#					special_type_slot_4 = afv_battlestation_3
#				}
#				upgrades = {
#					afv_nsb_armor_upgrade = 4
#				}
#				icon = "GFX_ifv_hull_3_a"
#			}
#		}
#
#		ai_will_do = { factor = 0 }
#	}

	# JAP
	type_10_decision = {
		icon = GFX_decision_type10_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = JAP
		}

		available = {
			has_tech = medium_gun_tech_3
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_4
			has_tech = era_tech_3
			has_tech = mbt_tech_3
			has_tech = armor_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision type_10_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "Type 10"
				type = mbt_hull_3
				modules = {
					main_armament_slot = tank_medium_cannon_3
					turret_type_slot = tank_base_tank_turret
					suspension_type_slot = tank_pneumatic_suspension
					armor_type_slot = tank_composite_armor_gen3
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = hardkill_system_gen1
					special_type_slot_4 = tank_battlestation_4
					special_type_slot_5 = reactive_armor_gen3
				}
				upgrades = {
					tank_nsb_armor_upgrade = 1
				}
				icon = "GFX_JAP_MBT_5"
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	# POL
	rosomak_decision = {
		icon = GFX_decision_rosomak_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = POL
		}

		available = {
			has_tech = tank_components_tech_2
			has_tech = afv_tech_2
			has_tech = small_autocannon_tech_2
			has_tech = diesel_engine_tech_3
			has_tech = small_autocannon_tech_2
			has_tech = armor_tech_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision rosomak_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "KTO Rosomak"
				type = ifv_hull_2
				modules = {
					main_armament_slot = small_autocannon_2
					turret_type_slot = afv_unmanned_turret
					suspension_type_slot = afv_torsion_bar_suspension
					armor_type_slot = afv_steel_armor_gen2
					engine_type_slot = tank_diesel_engine_gen3
					special_armament_type_slot = afv_coax_machine_gun_2
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = afv_atgm_gen2
					special_type_slot_3 = afv_additional_machine_guns
					special_type_slot_4 = afv_battlestation_2
					special_slot_type_5 = spaced_armor_gen2
				}
				upgrades = {
					afv_nsb_armor_upgrade = 3
				}
				icon = "GFX_POL_APC_5"
			}
		}

		ai_will_do = { factor = 10 }
	}

	# ISR
	namer_decision = {
		icon = GFX_decision_namer_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = ISR
		}

		available = {
			has_tech = infantry_weapons4
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = afv_tech_2
			has_tech = armor_tech_2
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision namer_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "Namer"
				type = apc_hull_2
				modules = {
					main_armament_slot = afv_machine_gun_3
					turret_type_slot = afv_unmanned_turret
					suspension_type_slot = afv_torsion_bar_suspension
					armor_type_slot = afv_composite_armor_gen3
					engine_type_slot = tank_diesel_engine_gen5
					special_armament_type_slot = empty
					special_type_slot_1 = additional_machine_guns
					special_type_slot_2 = smoke_launchers_2
					special_type_slot_3 = empty
					special_type_slot_4 = afv_battlestation_2
				}
				upgrades = {
					afv_nsb_armor_upgrade = 4
				}
				icon = "GFX_apc_hull_3_a"
			}
		}

		ai_will_do = { factor = 10 }
	}

	eitan_decision = {
		icon = GFX_decision_eitan_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = ISR
		}

		available = {
			has_tech = infantry_weapons4
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = afv_tech_2
			has_tech = armor_tech_3
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision eitan_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "Eitan"
				type = apc_hull_3
				modules = {
					main_armament_slot = afv_machine_gun_3
					turret_type_slot = afv_unmanned_turret
					suspension_type_slot = afv_tracked_suspension
					armor_type_slot = afv_alum_armor_gen3
					engine_type_slot = tank_diesel_engine_gen4
					special_armament_type_slot = empty
					special_type_slot_1 = additional_machine_guns
					special_type_slot_2 = smoke_launchers_2
					special_type_slot_3 = hardkill_system_gen1
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 4
				}
				icon = "GFX_ISR_APC_5"
			}
		}

		ai_will_do = { factor = 10 }
	}

	# UKR
	bulat_decision = {
		icon = GFX_decision_t64_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 19
		}
		custom_cost_text = army_xp_20

		visible = {
			original_tag = UKR
		}

		available = {
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = medium_gun_tech_2
			has_tech = Heavy_Anti_tank_1
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision bulat_decision"
			army_experience = -20
			create_equipment_variant = {
				name = "T-64BM2 'Bulat'"
				type = mbt_hull_1
				modules = {
					main_armament_slot = tank_medium_cannon_2
					turret_type_slot = tank_base_tank_turret
					suspension_type_slot = tank_torsion_bar_suspension
					armor_type_slot = tank_composite_armor_gen2
					engine_type_slot = tank_diesel_engine_gen5
					reload_type_slot = automatic_loading
					special_type_slot_1 = smoke_launchers_2
					special_type_slot_2 = additional_machine_guns
					special_type_slot_3 = smoothbore_atgm_gen2
					special_type_slot_4 = tank_battlestation_3
					special_type_slot_5 = reactive_armor_gen2
				}
				upgrades = {
					tank_nsb_armor_upgrade = 4
				}
				icon = "GFX_SOV_MBT_3"
			}
		}

		ai_will_do = { factor = 10 }
	}

	btr4_decision = {
		icon = GFX_decision_btr4_button

		fire_only_once = yes
		custom_cost_trigger = {
			has_army_experience > 34
		}
		custom_cost_text = army_xp_35

		visible = {
			original_tag = UKR
		}

		available = {
			has_tech = diesel_engine_tech_5
			has_tech = tank_components_tech_3
			has_tech = small_autocannon_tech_2
			has_tech = Heavy_Anti_tank_1
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision btr4_decision"
			army_experience = -35
			create_equipment_variant = {
				name = "BTR-4"
				type = ifv_hull_2
				modules = {
					main_armament_slot = small_autocannon_2
					turret_type_slot = afv_unmanned_turret
					suspension_type_slot = afv_tracked_suspension
					armor_type_slot = afv_steel_armor_gen2
					engine_type_slot = tank_diesel_engine_gen3
					special_armament_type_slot = afv_coax_machine_gun_2
					special_type_slot_1 = smoke_launchers_2
					special_slot_type_2 = afv_atgm_gen2
					special_slot_type_3 = additional_machine_guns
					special_type_slot_4 = afv_battlestation_3
				}
				upgrades = {
					afv_nsb_armor_upgrade = 3
				}
				icon = "GFX_ifv_hull_1_a"
			}
		}

		ai_will_do = { factor = 10 }
	}
}