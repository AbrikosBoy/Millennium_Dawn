USA_congress_md = {
	icon = usa_congress
	allowed = { original_tag = USA }
	scripted_gui = usa_congress_decision_ui
	priority = 150
}

USA_american_economic_decisions_category = {
	icon = GFX_decision_category_generic_economy
	picture = GFX_decision_cat_generic_stock_market
	allowed = { original_tag = USA }

	priority = 145
}

war_on_terror_category = {
	icon = infiltration
	allowed = { original_tag = USA }

	visible = {
		original_tag = USA
	}

	priority = 140
}

USA_the_trump_agenda = {
	icon = political_actions
	allowed = { original_tag = USA }
	visible = {
		has_country_leader = { name = "Donald Trump" ruling_only = yes }
	}
	priority = 140
}

USA_america_first = {
	icon = political_actions
	allowed = { original_tag = USA }
	visible = {
		has_country_leader = { name = "Donald Trump" ruling_only = yes }
	}
	priority = 140
}
USA_blockade_america_category = {
	icon = GFX_decision_usa
	   priority = 120
	   allowed = {
		   original_tag = USA
	   }
	   visible = {
		has_country_flag = blockade_cuba_yes
   }
}
#Asteroid Mining, USA and others can use it, but the USA is the one who actually "sets it off"
USA_asteroid_mining_decision = {
	icon = political_actions

	visible = {
		OR = {
			AND = {
				has_global_flag = asteroid_mining
				num_of_factories > 40
			}
			AND = {
				has_country_flag = asteroid_mining
				original_tag = USA
			}
		}

	}
	priority = 140
}

USA_decisions_domestic_policies = {
	icon = political_actions
	allowed = { original_tag = USA }
	priority = 149
}

USA_political_and_economic_reforms = {
	icon = usa_congress
	allowed = { original_tag = USA }
	priority = 148
}