SOO_revoult_south_caucas = {
	allowed = {
		original_tag = SOO
	}
	priority = 90
	picture = GFX_decision_russia_star
	visible = {
		emerging_communist_state_are_in_power = yes
		has_completed_focus = SOO_revolutions_in_southcauc
	}
}
SOO_greater_skifia_decisions_category = {
	allowed = {
		original_tag = SOO
	}
	priority = 90
	icon = GFX_decisions_category_skufia
	picture = GFX_decision_greater_scythia
	visible = {
		has_government = nationalist
		has_completed_focus = SOO_eurasian_ambitions
	}
}