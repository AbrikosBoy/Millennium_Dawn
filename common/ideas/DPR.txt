#Made by Lord Bogdanoff
ideas = {
	country = {
		NOV_novorossia = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOV_novorossia" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			cancel = {
				OR = {
					DPR = {	is_subject = yes }
					NOT = { country_exists = DPR }
					LPR = {	is_subject = yes }
					NOT = { country_exists = LPR }
				}
			}
			removal_cost = -1
			picture = nov_novorossiya
			modifier = {
				political_power_factor = 0.02
			}
		}
		NOV_novorossia1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOV_novorossia1" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			name = NOV_novorossia
			cancel = {
				OR = {
					DPR = {	is_subject = yes }
					NOT = { country_exists = DPR }
					LPR = {	is_subject = yes }
					NOT = { country_exists = LPR }
				}
			}
			removal_cost = -1
			picture = nov_novorossiya
			modifier = {
				political_power_factor = 0.02
				army_attack_factor = 0.05
				army_defence_factor = 0.05
				army_org_factor = 0.10
			}
		}
		DPR_civil_war = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_civil_war" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			cancel = {
				OR = {
					SOV = { has_war_with = UKR }
					has_autonomy_state = autonomy_republic_rf
					is_subject_of = UKR
					NOT = { has_war_with = UKR }
				}
			}
			removal_cost = -1
			picture = dpr_civilwar
			targeted_modifier = {
				tag = UKR
				attack_bonus_against = -0.10
				defense_bonus_against = 0.50
			}
		}
		LPR_unproffesional_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add LPR_unproffesional_army " }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = bad_army_idea
			modifier = {
				army_defence_factor = -0.30
				army_attack_factor = -0.30
				training_time_army_factor = -0.1
			}
		}
		LPR_proffesional_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add LPR_proffesional_army" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.2
			}
		}
		LPR_proffesional_army2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add LPR_proffesional_army2" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.1
			}
		}
		LPR_no_goverment_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea LPR_no_goverment_idea" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = lpr_government_idea
			modifier = {
				political_power_gain = -0.05
				foreign_influence_defense_modifier = -0.15
			}
		}
		LPR_ter_orobona = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add LPR_ter_orobona" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = lpr_teroborsona_idea
			modifier = {
				mobilization_speed = 0.04
				army_core_defence_factor = 0.03
				war_support_factor = 0.01
				training_time_army_factor = -0.02
			}
		}
		LPR_mgb = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add LPR_mgb" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = lpr_mgb_idea
			modifier = {
				encryption_factor = 0.5
				foreign_influence_defense_modifier = 0.10
			}
		}
		DPR_ter_orobona = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_ter_orobona" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = dpr_teroborsona_idea
			modifier = {
				mobilization_speed = 0.04
				army_core_defence_factor = 0.03
				war_support_factor = 0.01
				training_time_army_factor = -0.02
			}
		}
		DPR_unproffesional_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_unproffesional_army " }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = bad_army_idea
			modifier = {
				army_defence_factor = -0.30
				army_attack_factor = -0.30
				training_time_army_factor = -0.1
			}
		}
		DPR_proffesional_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_proffesional_army" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = dpr_profes_army
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.2
			}
		}
		DPR_proffesional_army1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_proffesional_army1" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = defense
			modifier = {
				initiative_factor = 0.05
				planning_speed = 0.10
				max_planning_factor = 0.10
			}
		}
		DPR_proffesional_army2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_proffesional_army2" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = defense
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.2
				send_volunteer_size = 2
				send_volunteer_divisions_required = -0.20
				send_volunteers_tension = -0.1
			}
		}
		DPR_donbass_ruins = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_donbass_ruins" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = dpr_ruins
			modifier = {
				stability_factor = -0.20
				consumer_goods_factor = 0.2
				production_speed_buildings_factor = -0.30
				industrial_capacity_factory = -0.2
				production_factory_max_efficiency_factor = -0.1
			}
		}
		DPR_donbass_ruins2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_donbass_ruins2" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = dpr_ruins
			modifier = {
				stability_factor = -0.10
				consumer_goods_factor = 0.15
				production_speed_buildings_factor = -0.20
				industrial_capacity_factory = -0.2
				production_factory_max_efficiency_factor = -0.1
			}
		}
		DPR_vstaia_donbass = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_vstaia_donbass" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = dpr_donbass_rise
			modifier = {
				stability_factor = 0.20
				war_support_factor = 0.20
				army_defence_factor = 0.05
				army_attack_factor = 0.05
			}
		}
		DPR_bivaluta = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_bivaluta" }
			picture = dpr_bivaluta
			allowed_civil_war = { always = yes }
			modifier = {
				stability_factor = -0.25
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5
			}
		}
		DPR_sport_development = {
			allowed = {
				original_tag = DPR
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}
			picture = dpr_sport_idea
			modifier = {
				stability_factor = 0.01
				monthly_population = 0.05
			}
		}
		DPR_fsb = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_fsb" }
			allowed_civil_war = { always = yes }
			picture = sov_fsb
			modifier = {
				encryption_factor = 0.15
				foreign_influence_defense_modifier = 0.10
			}
		}
		DPR_no_goverment_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_no_goverment_idea" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = dpr_government_idea
			modifier = {
				political_power_gain = -0.05
				foreign_influence_defense_modifier = -0.15
			}
		}
		DPR_mgb = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_mgb" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = dpr_mgb_idea
			modifier = {
				encryption_factor = 0.5
				foreign_influence_defense_modifier = 0.10
			}
		}
		DPR_anti_lgbt = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_anti_lgbt" }
			allowed = { always = no }
			picture = anti_lgbt
			modifier = {
				political_power_factor = 0.02
				democratic_drift = -0.05
				communism_drift = 0.04
			}
		}
		DPR_youth_front = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_youth_front" }
			allowed = { always = no }
			picture = dpr_policy_idea
			modifier = {
				conscription_factor = 0.075
				war_support_factor = 0.05
			}
		}
		DPR_vpk = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_vpk" }
			picture = dpr_vpk_idea
			modifier = {
				production_speed_arms_factory_factor = 0.10
				industrial_capacity_factory = 0.11
			}
		}
		DPR_vpk1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_vpk1" }
			picture = dpr_vpk_idea
			name = DPR_vpk
			modifier = {
				production_speed_arms_factory_factor = 0.10
				industrial_capacity_factory = 0.11
			}
			equipment_bonus = {
				Inf_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
			}
		}
		DPR_vpk2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_vpk2" }
			picture = dpr_vpk_idea
			name = DPR_vpk
			modifier = {
				production_speed_arms_factory_factor = 0.10
				industrial_capacity_factory = 0.11
			}
			equipment_bonus = {
				Inf_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
				artillery_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
			}
		}
		DPR_vpk3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_vpk3" }
			picture = dpr_vpk_idea
			name = DPR_vpk
			modifier = {
				production_speed_arms_factory_factor = 0.10
				industrial_capacity_factory = 0.11
			}
			equipment_bonus = {
				Inf_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
				artillery_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
				Kamikaze_drone_equipment = {
					build_cost_ic = -0.25
					instant = yes
				}
			}
		}
		DPR_russian_oil = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add DPR_russian_oil" }
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = blr_contract_good_sov_idea
			modifier = {
				country_resource_oil = 3
			}
		}
		DPR_rf_money = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea DPR_rf_money" }
			allowed_civil_war = {
				always = yes
			}
			allowed = {
				original_tag = DPR
				always = yes
			}
			cancel = {
				SOV = { has_opinion = { target = DPR value < 10 } }
			}
			picture = rus_capitalism
			modifier = {
				fuel_gain = 100
				industry_repair_factor = 0.08
				research_speed_factor = 0.04
			}
		}
		UKR_civil_war = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add UKR_civil_war" }
			allowed = {
				original_tag = UKR
			}
			allowed_civil_war = {
				always = yes
			}
			cancel = {
				OR = {
					SOV = { has_war_with = UKR }
					is_subject_of = SOV
					AND = {
						NOT = { country_exists = DPR }
						NOT = { country_exists = LPR }
						NOT = { country_exists = HPR }
						NOT = { country_exists = OPR }
					}
				}
			}
			removal_cost = -1
			picture = dpr_civilwar
			targeted_modifier = {
				tag = DPR
				attack_bonus_against = -0.48
				breakthrough_bonus_against = -0.6
				defense_bonus_against = 0.1
			}
			targeted_modifier = {
				tag = LPR
				attack_bonus_against = -0.48
				breakthrough_bonus_against = -0.6
				defense_bonus_against = 0.1
			}
		}
	}
}