ideas = {
	country = {
		HEZ_militant_group = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_militant_group" }
			allowed = {
			}

			picture = militant_group_soldier

			allowed_civil_war = {
				always = no
			}
			cancel = {
			}

			modifier = {
				communism_drift = 1.0
				personnel_cost_multiplier_modifier = -0.50
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.15
				weekly_manpower = 200
				production_speed_buildings_factor = -0.20
				political_power_factor = -0.20
			}
		}
		HEZ_militant_group_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_militant_group_2" }
			name = HEZ_militant_group
			picture = militant_group_soldier

			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 1.0
				personnel_cost_multiplier_modifier = -0.50
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.15
				weekly_manpower = 200
				production_speed_buildings_factor = -0.20
				political_power_factor = -0.20
				consumer_goods_factor = -0.05
			}
		}
		HEZ_militant_group_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_militant_group_3" }
			name = HEZ_militant_group

			picture = militant_group_soldier

			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 1.0
				personnel_cost_multiplier_modifier = -0.50
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.15
				weekly_manpower = 200
				production_speed_buildings_factor = -0.20
				political_power_factor = -0.20
				army_morale_factor = 0.10
				army_attack_factor = 0.10
			}
		}
		HEZ_militant_group_4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_militant_group_4" }
			name = HEZ_militant_group

			picture = militant_group_soldier

			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 1.0
				personnel_cost_multiplier_modifier = -0.50
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.15
				weekly_manpower = 200
				production_speed_buildings_factor = -0.20
				political_power_factor = -0.20
				army_morale_factor = 0.10
				army_attack_factor = 0.10
				recruitable_population = 0.02
			}
		}
		HEZ_militant_group_5 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_militant_group_5" }
			name = HEZ_militant_group

			picture = militant_group_soldier

			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 1.0
				personnel_cost_multiplier_modifier = -0.50
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.15
				weekly_manpower = 200
				production_speed_buildings_factor = -0.20
				political_power_factor = -0.20
				army_morale_factor = 0.10
				army_attack_factor = 0.10
				recruitable_population = 0.02
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.10
			}
		}
		HEZ_militant_group_6 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_militant_group_6" }
			name = HEZ_militant_group

			picture = militant_group_soldier

			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 1.0
				personnel_cost_multiplier_modifier = -0.50
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.15
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.15
				weekly_manpower = 200
				production_speed_buildings_factor = -0.20
				political_power_factor = -0.20
				army_morale_factor = 0.10
				army_attack_factor = 0.10
				recruitable_population = 0.02
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.10
				production_speed_offices_factor = 0.1
				production_speed_infrastructure_factor = 0.1
				production_speed_industrial_complex_factor = 0.1
			}
		}
		HEZ_hezbollah_influencing_lebanon = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_influencing_lebanon" }
			picture = iranian_aid
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			}

			modifier = {
				communism_drift = 0.2
			}
		}
		HEZ_the_LAF = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_the_LAF" }
			picture = shared_research
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			}

			modifier = {
				research_speed_factor = 0.05
			}
		}
		HEZ_public_support = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_public_support" }
			picture = political_support
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			NOT = { country_exists = LEB }
			}

			modifier = {
				political_power_gain = 1.0
			}
		}
		HEZ_hezbollah_armed_forces = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces" }
			picture = Disorganization_Military_5
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
			}
		}
		HEZ_hezbollah_armed_forces_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_2" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.1
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.1
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.1
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.1
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
			}
		}
		HEZ_hezbollah_armed_forces_3_A = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_3_A" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.1
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.1
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.1
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.1
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
			}
			equipment_bonus = {
				L_AT_Equipment = {
					hard_attack = 0.05
					instant = yes
				}
			}
			equipment_bonus = {
				Inf_equipment = {
					soft_attack = 0.05
					instant = yes
				}
			}
		}
		HEZ_hezbollah_armed_forces_3_B = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_3_B" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				modifier_army_sub_unit_Militia_Bat_attack_factor = 0.1
				modifier_army_sub_unit_Militia_Bat_defence_factor = 0.1
				modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.1
				modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.1
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				conscription = 0.05
				army_org_factor = 0.15
				army_morale_factor = 0.15
			}
			equipment_bonus = {
				L_AT_Equipment = {
					hard_attack = 0.05
					instant = yes
				}
			}
			equipment_bonus = {
				Inf_equipment = {
					soft_attack = 0.05
					instant = yes
				}
			}
		}
		HEZ_hezbollah_armed_forces_4_A = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_4_A" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
			modifier_army_sub_unit_Militia_Bat_attack_factor = 0.1
			modifier_army_sub_unit_Militia_Bat_defence_factor = 0.1
			modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.1
			modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.1
			army_core_attack_factor = 0.1
			army_core_defence_factor = 0.1
			conscription = 0.05
			army_org_factor = 0.15
			army_morale_factor = 0.15
			}
		}
		HEZ_hezbollah_armed_forces_4_B = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_4_B" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
			modifier_army_sub_unit_Militia_Bat_attack_factor = 0.1
			modifier_army_sub_unit_Militia_Bat_defence_factor = 0.1
			modifier_army_sub_unit_Mot_Militia_Bat_attack_factor = 0.1
			modifier_army_sub_unit_Mot_Militia_Bat_defence_factor = 0.1
			army_core_attack_factor = 0.1
			army_core_defence_factor = 0.1
			conscription = 0.05
			army_org_factor = 0.15
			army_morale_factor = 0.15
			}
			equipment_bonus = {
				L_AT_Equipment = {
					hard_attack = 0.05
					instant = yes
				}
			}
			equipment_bonus = {
				Inf_equipment = {
					soft_attack = 0.05
					instant = yes
				}
			}
		}
		HEZ_hezbollah_armed_forces_5_A = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_5_A" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				conscription = 0.15
			}
		}
		HEZ_hezbollah_armed_forces_5_B = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_5_B" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				weekly_manpower = 100
				conscription = 0.15
			}
		}
		HEZ_hezbollah_armed_forces_5_C = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_5_C" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				weekly_manpower = 100
				own_operative_capture_chance_factor = -0.15
				intel_network_gain_factor = 0.25
				conscription = 0.15
			}
		}
		HEZ_hezbollah_armed_forces_6_A = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_6_A" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				weekly_manpower = 100
			}
		}
		HEZ_hezbollah_armed_forces_6_B = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_6_B" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				conscription = 0.15
				weekly_manpower = 100
			}
		}
		HEZ_hezbollah_armed_forces_7 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_7" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				conscription = 0.15
				weekly_manpower = 100
				land_night_attack = 0.1
				initiative_factor = 0.1
				terrain_penalty_reduction = 0.1
			}
		}

		HEZ_hezbollah_armed_forces_8 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_8" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {

			}
		}

		HEZ_hezbollah_armed_forces_9 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_9" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				training_time_factor = 0.1
				army_org_factor = 0.1
			}
		}

		HEZ_hezbollah_armed_forces_10 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_armed_forces_10" }
			picture = Disorganization_Military_5
			name = HEZ_hezbollah_armed_forces
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
				training_time_factor = 0.1
				army_org_factor = 0.1
			}
			equipment_bonus = {
				AS_Fighter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				Strike_fighter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				L_Strike_fighter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				CV_L_Strike_fighter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				MR_Fighter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				CV_MR_Fighter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				CAS_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				nav_plane_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				strategic_bomber_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				transport_plane_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				attack_helicopter_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
				Air_UAV_equipment = {
					instant = yes
					build_cost_ic = -0.10
				}
			}
		}

		HEZ_secret_service = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_secret_service" }
			picture = spy_intel
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				own_operative_capture_chance_factor = -0.15
				intel_network_gain_factor = 0.25
			}
		}

		HEZ_secret_service_2 = {
			name = HEZ_secret_service
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_secret_service" }
			picture = spy_intel
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				own_operative_capture_chance_factor = -0.15
				intel_network_gain_factor = 0.25
				operative_slot = 3
			}
		}

		HEZ_council_of_12 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_council_of_12" }
			picture = political_power_bonus
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			}

			modifier = {
				political_power_factor = 0.1
				political_power_gain = 0.2
			}
		}
		HEZ_blue_shirt_kids = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_blue_shirt_kids" }
			picture = Disorganization_Military_3
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			has_idea = draft_army
			}

			modifier = {
				war_support_factor = 0.1
				training_time_factor = -0.1
				communism_drift = 0.05
			}
		}
		HEZ_al_manar = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_al_manar" }
			picture = propaganda
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			name = HEZ_al_manar

			modifier = {
				stability_factor = 0.05
				communism_drift = 0.03
			}
		}
		HEZ_lujnat_imdad_al_khomeini = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_lujnat_imdad_al_khomeini" }
			picture = foreign_capital
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				war_support_factor = 0.1
				training_time_factor = -0.1
				communism_drift = 0.05
				political_power_factor = 0.1
				political_power_gain = 0.2
				health_cost_multiplier_modifier = -0.05
				social_cost_multiplier_modifier = -0.05
				education_cost_multiplier_modifier = -0.05
			}
		}
		HEZ_al_manar_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_al_manar_2" }
			name = propaganda
			picture = iranian_aid
			allowed = {
			}

			name = HEZ_al_manar
			allowed_civil_war = {
				always = no
			}

			modifier = {
			stability_factor = 0.05
			communism_drift = 0.06

			}
		}
		HEZ_hezbollah_is_watching_you = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_hezbollah_is_watching_you" }
			picture = iranian_aid
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 0.02
			}
		}
		HEZ_volunteers_of_all_beliefs = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_volunteers_of_all_beliefs" }
			picture = muslim3
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			has_idea = draft_army
			}

			modifier = {
				conscription_factor = 0.10
			}
		}
		HEZ_peoples_support_for_hezbollah_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_peoples_support_for_hezbollah_1" }
			picture = national_mobilization
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
				stability_factor = 0.10
				communism_drift = 0.20
				war_support_factor = 0.15
				training_time_factor = -0.1
				political_power_factor = 0.1
				political_power_gain = 0.3
				health_cost_multiplier_modifier = -0.05
				social_cost_multiplier_modifier = -0.05
				education_cost_multiplier_modifier = -0.05
				conscription_factor = 0.10
			}
		}
		HEZ_iranian_business = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_business" }
			picture = iranian_flag
			allowed = {
			}
			cancel = {
				NOT = {
					OR = {
						is_subject_of = PER
						PER = {
							OR = {
								emerging_hardline_shiite_are_in_power = yes
								emerging_moderate_shiite_are_in_power = yes
							}
						}
					}
				}
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
				production_speed_buildings_factor = 0.20
			}
		}
		HEZ_iranian_business_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_business_2" }
			picture = iranian_flag
			name = HEZ_iranian_business
			allowed = {
			}
			cancel = {
				NOT = {
					OR = {
						is_subject_of = PER
						PER = {
							OR = {
								emerging_hardline_shiite_are_in_power = yes
								emerging_moderate_shiite_are_in_power = yes
							}
						}
					}
				}
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
				production_speed_buildings_factor = 0.25
				production_speed_infrastructure_factor = 0.15
			}
		}
		HEZ_iranian_business_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_business_3" }
 			picture = iranian_flag
			name = HEZ_iranian_business
			allowed = {
			}
			cancel = {
				NOT = {
					OR = {
						is_subject_of = PER
						PER = {
							OR = {
								emerging_hardline_shiite_are_in_power = yes
								emerging_moderate_shiite_are_in_power = yes
							}
						}
					}
				}
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
				production_speed_buildings_factor = 0.20
				production_speed_industrial_complex_factor = 0.15
			}
		}
		HEZ_iranian_business_4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_business_4" }
			picture = iranian_flag
			name = HEZ_iranian_business
			allowed = {
			}
			cancel = {
				NOT = {
					OR = {
						is_subject_of = PER
						PER = {
							OR = {
								emerging_hardline_shiite_are_in_power = yes
								emerging_moderate_shiite_are_in_power = yes
							}
						}
					}
				}
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
				production_speed_buildings_factor = 0.20
				production_speed_infrastructure_factor = 0.15
				production_speed_industrial_complex_factor = 0.15
			}
		}
		HEZ_iranian_business_5 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_business_5" }
			picture = iranian_flag
			name = HEZ_iranian_business
			allowed = {
			}
			cancel = {
				NOT = {
					OR = {
						is_subject_of = PER
						PER = {
							OR = {
								emerging_hardline_shiite_are_in_power = yes
								emerging_moderate_shiite_are_in_power = yes
							}
						}
					}
				}
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
 				custom_modifier_tooltip = HEZ_Iranian_Business_Stimulus_modifier_TT
				production_speed_buildings_factor = 0.20
				production_speed_infrastructure_factor = 0.15
				production_speed_industrial_complex_factor = 0.15
				production_speed_offices_factor = 0.15
			}
		}
		HEZ_iranian_business_6 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_business_6" }
			picture = iranian_flag
			name = HEZ_iranian_business
			allowed = {
			}
			cancel = {
				NOT = {
					OR = {
						is_subject_of = PER
						PER = {
							OR = {
								emerging_hardline_shiite_are_in_power = yes
								emerging_moderate_shiite_are_in_power = yes
							}
						}
					}
				}
			}
			allowed_civil_war = {
				always = no
			}
			modifier = {
 				custom_modifier_tooltip = HEZ_Iranian_Business_Stimulus_modifier_TT
				research_speed_factor = 0.10
				production_speed_buildings_factor = 0.20
				production_speed_infrastructure_factor = 0.15
				production_speed_industrial_complex_factor = 0.15
				production_speed_offices_factor = 0.15
			}
		}
		HEZ_wave_of_volunteers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_wave_of_volunteers" }
			picture = iran_fighter_for_nation
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			cancel = {
			has_idea = draft_army
			}

			modifier = {
				weekly_manpower = 200
			}
		}
		HEZ_the_second_khatam_al_anbia_construction_camp = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_the_second_khatam_al_anbia_construction_camp" }
			picture = construction
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = 0.1
				consumer_goods_factor = -0.05
			}
		}
		HEZ_foreign_economical_funds = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_foreign_economical_funds" }
			picture = positive_gold
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				consumer_goods_factor = -0.035
				production_factory_efficiency_gain_factor = 0.05
			}
		}
		HEZ_liberization = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_liberization" }
			picture = market_liberalism
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				research_speed_factor = 0.10
				production_factory_efficiency_gain_factor = 0.05
			}
		}
		HEZ_industrial_investments = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_industrial_investments" }
			picture = industrial_focus
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.1
			}
		}
		HEZ_eastern_propaganda = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_eastern_propaganda" }
			picture = iranian_aid
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				communism_drift = 0.05
			}
		}
		HEZ_preparing_for_new_plans = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_preparing_for_new_plans" }
			picture = spy_political
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				weekly_manpower = -150
				political_power_factor = -0.25
				stability_factor = -0.05
			}
		}
		HEZ_strange_activities_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_strange_activities_1" }
			picture = spy_political
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_weekly_factor = -0.0010
				political_power_factor = -0.1
				war_support_weekly_factor = -0.0010
			}

			}
		HEZ_strange_activities_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_strange_activities_2" }
			picture = spy_political
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_weekly_factor = -0.0010
				political_power_factor = -0.1
				war_support_weekly_factor = -0.0010
				weekly_manpower = -20
				experience_gain_army_factor = -0.05
			}
		}
		HEZ_defenders_of_the_shrine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_defenders_of_the_shrine" }
			picture = manpower_bonus
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				war_support_weekly_factor = 0.001
				weekly_manpower = 100
			}
			targeted_modifier = {
				tag = ISI
				attack_bonus_against = 0.15
				defense_bonus_against = 0.15
			}
		}
		HEZ_our_cartels_in_south_america = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_our_cartels_in_south_america" }
			picture = cartels
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				political_power_factor = 0.15
				weekly_manpower = -10
			}
		}
		HEZ_unknown_cartel_operations = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_unknown_cartel_operations" }
			picture = cartels
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				political_power_factor = -0.25
				stability_weekly_factor = -0.002
				police_cost_multiplier_modifier = 0.20
			}
		}
		HEZ_naim_qassim = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_naim_qassim" }
			picture = low_popular_support
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = 0.03
				army_core_attack_factor = 0.05
				army_core_defence_factor = 0.05
				political_power_factor = 0.15
			}
		}
		HEZ_the_last_push = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_the_last_push" }
			picture = volunteer_expedition_bonus
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				war_support_weekly_factor = 0.001
				weekly_manpower = 100
			}
			targeted_modifier = {
				tag = ISR
				attack_bonus_against = 0.15
				defense_bonus_against = 0.20
			}
		}
		HEZ_iranian_missile_aid = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HEZ_iranian_missile_aid" }
			picture = volunteer_expedition_bonus
			allowed = {
			}
			allowed_civil_war = {
				always = no
			}

			modifier = {
				icbm_production_speed_modifier = 0.20
				irbm_production_speed_modifier = 0.15
				slbm_production_speed_modifier = 0.15
				alcm_production_speed_modifier = 0.15
				glcm_production_speed_modifier = 0.15
				slcm_production_speed_modifier = 0.15
				hscm_production_speed_modifier = 0.15
				sam_production_speed_modifier = 0.15
				abm_production_speed_modifier = 0.15
			}
		}
	}
	hidden_ideas = {
		hezbollah_influence_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea hezbollah_influence_idea" }
			allowed = {
			}
				allowed_civil_war = {
				always = no
			}
				cancel = {
			}
			modifier = {
				communism_drift = 0.1
				# Needs modifier to increase Shiitee revulotionaries
			}
		}
	}
}