
ai_focus_defense_SOV = {
	research = {
		defensive = 5.0
		CAT_artillery = 5.0
		CAT_inf_wep = 1.0
	}
}

ai_focus_aggressive_SOV = {
	research = {
		offensive = 5.0
		CAT_armor = 5.0
	}
}

ai_focus_war_production_SOV = {
	research = {
		CAT_construction_tech = 55.0
		CAT_fuel_oil = 12.0
		CAT_nfibers = 55.0
		CAT_3d = 55.0
		CAT_ai = 55.0
		CAT_genes = 20.0
		CAT_excavation_tech = 4.0
		CAT_infrastructure = 3.0
	}
}

ai_focus_military_equipment_SOV = {
	research = {
		CAT_land_doctrine = 5
		CAT_eastern = 40
		CAT_airmobile = 30
		CAT_airborne = 20
		CAT_tech_doctrines = 10
		CAT_offensive_doctrine = 30
		CAT_defensive_doctrine = 10
		CAT_unconventional = 5
		CAT_training = 5
		CAT_equipment_doctrines = 40

		CAT_inf = 5.0
		CAT_inf_wep = 25.0
		CAT_nvg = 10.0
		CAT_cnc = 10.0
		CAT_support_weapons = 10.0
		CAT_special_forces = 10.0

		CAT_at = 15.0
		CAT_aa = 15.0

		Cat_ENG_EQP = 1.0

		CAT_artillery = 8.0
	}
}

ai_focus_military_advancements_SOV = {
	research = {
		CAT_eastern = 10.0

		CAT_inf = 3.0
		CAT_l_drone = 2.0
		CAT_special_forces = 8.0
		CAT_airmobile = 40
		CAT_airborne = 30
		CAT_support_weapons = 20.0
		CAT_nvg = 15.0

		CAT_armor = 22.5
		CAT_mbt = 4.0
		CAT_eng_mbt = 1.0
		CAT_apc = 6.0
		CAT_ifv = 6.0
		CAT_rec_tank = 2.0

		CAT_util = 5.0

		CAT_heli = 2.5
	}
}

ai_focus_peaceful_SOV = {
	research = {
		CAT_industry = 10.0
		CAT_ai = 20.0
		CAT_nfibers = 15.0
		CAT_3d = 15.0
		CAT_internet_tech = 14.0
		CAT_computing_tech = 14.0
		CAT_construction_tech = 20.0
		CAT_excavation_tech = 15.0
		CAT_genes = 12.0
		CAT_fuel_oil = 12.0
		CAT_infrastructure = 3
		CAT_nuclear_reactors = 4
	}
}

ai_focus_naval_SOV = {
	research = {
		CAT_green_water_navy = 2.0

		CAT_naval_eqp = 7.0
		CAT_naval_misc = 5.0

		CAT_n_cruiser = 0.0
		CAT_m_cruiser = 2.0
		CAT_destroyer = 4.0
		CAT_frigate = 8.0
		CAT_corvette = 8.0
		Cat_TRANS_SHIP = 6.0

		CAT_n_cv = 0.0
		CAT_cv = 1.0
		CAT_lha = 8.0
		CAT_lpd = 4.0

		CAT_d_sub = 6.0
		CAT_atk_sub = 0.0
		CAT_m_sub = 0.0
	}
}

ai_focus_naval_air_SOV = {
	research = {
		CAT_naval_air = 25.0
	}
}

ai_focus_aviation_SOV = {
	research = {
		CAT_air_doctrine = 6.0

		CAT_air_eqp = 15.0
		CAT_air_spc = 8.0
		CAT_air_wpn = 8.0

		CAT_h_air = 5.0
		CAT_str_bomber = 7.5
		CAT_cas = 10.0
		CAT_trans_plane = 5.0

		CAT_heli = 2.0
		CAT_trans_heli = 10.0
		CAT_atk_heli = 5.0

		CAT_fighter = 25.0
		CAT_mr_fighter = 8.0
		CAT_s_fighter = 1.0
		CAT_l_s_fighter = 1.0
		CAT_as_fighter = 20.0
		CAT_a_uav = 10.0
	}
}
