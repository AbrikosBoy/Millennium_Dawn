##
PER_sau_oil_strike = {
	enable = { always = yes }
	icon = GFX_idea_factory_strikes
	state_resources_factor = -0.30
	resistance_growth = 0.10
	temporary_state_resource_oil = sabotaged_oil
}
PER_sau_oil_strike2 = {
	enable = { always = yes }
	icon = GFX_idea_factory_strikes
	state_resources_factor = -0.40
	resistance_growth = 0.15
	local_building_slots_factor = -0.10
	temporary_state_resource_oil = sabotaged_oil
}
PER_sau_oil_strike3 = {
	enable = { always = yes }
	icon = GFX_idea_factory_strikes
	state_resources_factor = -0.50
	resistance_growth = 0.20
	local_building_slots_factor = -0.15
	temporary_state_resource_oil = sabotaged_oil
}
PER_IRGC_PRESENT = {
	enable = { always = yes }
	icon = GFX_idea_fire
	local_org_regain = -0.05
	local_building_slots_factor = -0.25
	state_production_speed_buildings_factor = -0.50
	army_speed_factor_for_controller = -0.20
	state_resources_factor = -0.50
	attrition_for_controller = 0.10
}
### Dynamic
PER_cult_of_rajavi = {
	enable = {
		original_tag = PER
		has_completed_focus = PER_rajavi_cult
	}
	political_power_gain = rajavi_cult
	communism_drift = rajavi_cult
}

PER_defensive_measures_modifier = {
	enable = { always = yes }
	icon = GFX_idea_national_defenders
	enemy_army_speed_factor = -0.15
	enemy_local_supplies = -0.15
	enemy_attrition = 0.15
}

PER_chahbahar_port = {
	enable = { always = yes }
	icon = GFX_idea_coastal_defense_ships
	local_building_slots = 5
	local_supply_impact_factor = -0.50
	local_supplies = 0.25
}