﻿UZB_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { UZB }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "UZB_ARMOURED_DIVISIONS" }

	fallback_name = "%d Piyodalar diviziyasi"

	ordered = {
		387 = { "%d-Havo-desant o'quv polki" }
	}
}

UZB_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { UZB }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Piyodalar brigadasi"
}

UZB_MECHANIZED_DIVISIONS = {
	name = "Mechanized Division"
	for_countries = { UZB }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Mexaniklashtirilgan bo'lim"

	ordered = {
		1 = { "%d-Motorli miltiq brigadasi" }
		2 = { "%d-Motorli miltiq brigadasi" }
		3 = { "%d-Motorli miltiq brigadasi" }
		25 = { "%d-Motorli miltiq brigadasi" }
		37 = { "%d-Motorli miltiq brigadasi" }
	}
}

UZB_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { UZB }

	division_types = { "armor_Bat" }

	link_numbering_with = { "UZB_ARMY_DIVISIONS" }

	fallback_name = "%d Tank bo'limi"
}


UZB_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { UZB }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Havo-desant brigadalari"

	ordered = {
		17 = { "%d-Havo hujumi brigadasi" }
	}
}

UZB_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { UZB }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Amfibiya tez yordam brigadasi"
}

UZB_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { UZB }

	division_types = { "Special_Forces" }

	fallback_name = "%d Maxsus kuchlar guruhlari"

	ordered = {
		15 = { "%d-Mustaqil maxsus kuchlar brigadasi" }
	}
}

UZB_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { UZB }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Mintaqaviy mudofaa brigadalari"
}
