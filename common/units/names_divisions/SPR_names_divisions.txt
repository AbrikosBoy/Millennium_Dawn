﻿SPR_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { SPR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Division"

	ordered = {
		1 = { "Division "Castillejos"" }
		2 = { "Division "San Marcial"" }
	}
}

SPR_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { SPR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Brigade"

	ordered = {
		1 = { "Brigade "Aragón" I" }
		2 = { "Brigade "Rey Alfonso XIII" II of the Legion" }
		3 = { "Brigade "Galicia" VII" }
		4 = { "Brigade "Guzmán el Bueno" X" }
		5 = { "Brigade "Extremadura" XI" }
		6 = { "Brigade "Guadarrama" XII" }
	}
}

SPR_MECHANIZED_DIVISIONS = {
	name = "Mechanized Division"
	for_countries = { SPR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%s Protected Infantry Brigade"

}

SPR_CAVALRY_REGIMENT = {
	name = "Cavalry Regiment"

	for_countries = { SPR }

	division_types = { "armor_Bat" }

	link_numbering_with = { "SPR_ARMY_DIVISIONS" }

	fallback_name = "%d Regimiento de Caballería "
}

SPR_PARATROOPERS_BRIGADES = {
	name = "Paratroopers Brigades"

	for_countries = { SPR }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "Brigada %d de Paracaidistas"
}


SPR_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { SPR }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Army Airmobile Brigades"

}

SPR_MAR_BRIGADES = {
	name = "Marine Infantry Brigades"

	for_countries = { SPR }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Brigada de Infantería de Marina"
}

SPR_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { SPR }

	division_types = { "Special_Forces" }

	fallback_name = "%d Grupos de Operaciones Especiales"

	# Unique Names for Special Operations Groups
	ordered = {
		2 = { "Grupo de Operaciones Especiales "Granada" II" }
		3 = { "Grupo de Operaciones Especiales "Valencia" III" }
		4 = { "Grupo de Operaciones Especiales "Tercio del Ampurdán" IV" }
		19 = { "Grupo de Operaciones Especiales "Maderal Oleaga" XIX" }
	}
}

# Generic Conscript/Militia Brigades
SPR_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { SPR }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Brigadas Regionales de Defensa"
}
