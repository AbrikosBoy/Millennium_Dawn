﻿EGY_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { EGY }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "EGY_ARMOURED_DIVISIONS" }

	fallback_name = "%d Aqsam Aljaysh"
}

EGY_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { EGY }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Katayib Aljaysh"
}

EGY_MECHANIZED_DIVISIONS = {
	name = "Mechanized Division"
	for_countries = { EGY }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%s Alqism Alali"

}

EGY_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { EGY }

	division_types = { "armor_Bat" }

	link_numbering_with = { "EGY_ARMY_DIVISIONS" }

	fallback_name = "%d Aqsam Aldabaabat"
}


EGY_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { EGY }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Alwiat Alhujum Aljawiyi"

}

EGY_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { EGY }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Alwiat Alantishar Alsarie Albarmayiya"
}

EGY_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { EGY }

	division_types = { "Special_Forces" }

	fallback_name = "%d Majmueat Alquaat Alkhasa"
}

EGY_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { EGY }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Alwiat Aldifae Al'iiqlimia"
}
