#Written by Hiddengearz, Balanced by Bird, re-balanced by dc
###Reworked as of 11/27/2020 adding stealth qualities and survivability - lowering damage output
equipments = {
	strategic_bomber_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = strategic_bomber
		group_by = archetype
		sprite = strat_bomber
		air_map_icon_frame = 8
		interface_category = interface_category_air

		allowed_types = {
			strategic_bomber
		}
		allow_mission_type = {
			strategic_bomber
			training
		}

		# Strategic bomber
		interface_overview_category_index = 5

		upgrades = {
			plane_bomb_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 1
		reliability = 0.80

		# Air vs Navy - high damage / low hit chance / hard to hurt
		naval_strike_attack = 11.75
		naval_strike_targetting = 6.25

		#Space taken in convoy
		lend_lease_cost = 12

		build_cost_ic = 650
		resources = {
			aluminium = 2
			rubber = 2
			tungsten = 2
		}
		fuel_consumption = 0.2
		manpower = 85
	}


	# Transport plane
	transport_plane_equipment = {
		is_archetype = yes
		is_convertable = yes
		type = air_transport
		group_by = archetype
		sprite = transport_plane
		air_map_icon_frame = 12
		interface_category = interface_category_air

		allow_mission_type = {
			paradrop
			air_supply
			training
		}
		allowed_types = {
			air_transport
		}
		# Transport
		interface_overview_category_index = 6

		air_superiority = 0
		reliability = 0.85

		# Air vs Navy - high damage / low hit chance / hard to hurt
		naval_strike_attack = 0.0
		naval_strike_targetting = 0.0

		build_cost_ic = 400
		resources = {
			aluminium = 3
			rubber = 2
		}
		fuel_consumption = 12
		manpower = 75
	}
#
	CAS_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes

		type = { cas }
		group_by = archetype
		sprite = cas
		air_map_icon_frame = 7
		picture = archetype_CAS_equipment
		interface_category = interface_category_air
		allowed_types = {
			cas
		}
		allow_mission_type = {
			cas
			naval_bomber
			port_strike
			attack_logistics
			training
		}

		# CAS
		interface_overview_category_index = 0

		upgrades = {
			plane_cas_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 1
		reliability = 0.92

		# Air vs Ground
		air_ground_attack = 16

		# Air vs Navy - high damage / high hit chance / easy to hurt
		naval_strike_attack = 9.25
		naval_strike_targetting = 7

		#Space taken in convoy
		lend_lease_cost = 8

		build_cost_ic = 200
		resources = {
			aluminium = 2
			rubber = 1
		}
		fuel_consumption = 7.75
		manpower = 22
	}
	##Naval Plane
	nav_plane_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		type = { naval_bomber scout_plane }
		group_by = archetype
		sprite = nav_plane
		air_map_icon_frame = 10
		interface_category = interface_category_air
		allow_mission_type = {
			naval_bomber
			port_strike
			recon
			training
		}
		allowed_types = {
			naval_bomber
			scout_plane
		}
		# Naval bomber
		interface_overview_category_index = 2

		upgrades = {
			nav_gun_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 1
		reliability = 0.825
		air_ground_attack = 2

		# Air vs Navy - high damage / medium hit chance / easy to hurt
		naval_strike_attack = 18
		naval_strike_targetting = 12.5

		build_cost_ic = 360
		resources = {
			aluminium = 3
			rubber = 2
		}
		fuel_consumption = 12
		manpower = 50
	}
	#AWACs
	awacs_equipment = {
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		picture = archetype_strat_bomber_equipment
		type = { scout_plane }
		group_by = archetype
		sprite = scout_plane
		air_map_icon_frame = 13
		allowed_types = {
			scout_plane
		}
		allow_mission_type = {
			recon
			training
		}

		interface_category = interface_category_air

		# scout_plane
		interface_overview_category_index = 7

		upgrades = {
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 0
		reliability = 0.8

		naval_strike_attack = 0
		naval_strike_targetting = 0

		resources = {
			aluminium = 2
			rubber = 1
		}

		manpower = 40
		fuel_consumption = 0.26
	}
#	##CV AWACs
	cv_awacs_equipment = {
		is_archetype = yes
		is_convertable = yes
		picture = archetype_strat_bomber_equipment
		carrier_capable = yes
		is_buildable = no
		type = { scout_plane }
		group_by = archetype
		sprite = scout_plane
		air_map_icon_frame = 13
		allow_mission_type = {
			recon
			training
		}
		allowed_types = {
			scout_plane
		}

		interface_category = interface_category_air

		# scout_plane
		interface_overview_category_index = 7

		upgrades = {
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 0
		reliability = 0.8

		naval_strike_attack = 0
		naval_strike_targetting = 0

		resources = {
			aluminium = 2
			rubber = 1
		}

		manpower = 40
		fuel_consumption = 0.26
	}

	Kamikaze_drone_equipment = {
		year = 1985
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		one_use_only = yes

		type = { cas }
		group_by = archetype
		sprite = cas
		air_map_icon_frame = 7
		picture = archetype_CAS_equipment
		interface_category = interface_category_air
		allowed_types = {
			cas
		}
		allow_mission_type = {
			cas
			attack_logistics
			training
		}

		interface_overview_category_index = 0

		upgrades = {
			plane_cas_upgrade
			plane_range_upgrade
			plane_engine_upgrade
			plane_reliability_upgrade
		}

		air_superiority = 0.5
		reliability = 0.8

		# Air vs Ground
		air_ground_attack = 40

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 20
		resources = {
			aluminium = 1
			steel = 1
		}
		fuel_consumption = 1
		manpower = 4
	}
}