scripted_gui = {
	# open_market_value_expense_screen = {
	# 	context_type = player_context
	# 	window_name = "market_value_expense_display_open"
	# 	parent_window_name = "countryinternationalmarketview"

	# 	effects = {
	# 		open_market_value_expense_screen_click = {
	# 			if = {
	# 				limit = { NOT = { has_country_flag = open_market_money_value } }
	# 				set_country_flag = open_market_money_value
	# 				set_variable = { new_contract_calc_cost = 0 }
	# 			}
	# 			else = {
	# 				clr_country_flag = open_market_money_value
	# 				set_variable = { new_contract_calc_cost = 0 }
	# 			}
	# 		}
	# 	}
	# }
	market_value_expense = {
		context_type = player_context
		# parent_window_name = "countryinternationalmarketview"
		window_name = "market_value_expense_display"

		visible = { has_country_flag = open_market_money_value }

		effects = {
			close_market_screen_button_click = {
				if = {
					limit = { NOT = { has_country_flag = open_market_money_value } }
					set_country_flag = open_market_money_value
					set_variable = { new_contract_calc_cost = 0 }
				}
				else = {
					clr_country_flag = open_market_money_value
					set_variable = { new_contract_calc_cost = 0 }
				}
			}
			clear_contract_button_click = {
				set_variable = { new_contract_calc_cost = 0 }
			}
			infantry_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.inf_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			cnc_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.cnc_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			aa_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.aa_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			at_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.at_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			hat_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.hat_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			art_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.art_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			util_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.util_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			mbt_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.mbt_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			light_tank_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.light_tank_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			ifv_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.ifv_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			apc_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.apc_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			spart_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.spart_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			spaa_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.spaa_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			trans_helo_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.trans_helo_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			atk_helo_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.atk_helo_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			sml_pln_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.sml_pln_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			med_pln_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.med_pln_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			lrg_pln_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.lrg_pln_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			train_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.train_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			convoy_equipment_button_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.convoy_cost_mult }
				add_to_variable = { new_contract_calc_cost = temp_base_cost_track }
			}
			infantry_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.inf_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			cnc_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.cnc_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			aa_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.aa_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			at_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.at_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			hat_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.hat_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			art_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.art_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			util_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.util_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			mbt_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.mbt_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			light_tank_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.light_tank_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			ifv_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.ifv_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			apc_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.apc_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			spart_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.spart_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			spaa_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.spaa_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			trans_helo_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.trans_helo_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			atk_helo_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.atk_helo_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			sml_pln_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.sml_pln_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			med_pln_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.med_pln_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			lrg_pln_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.lrg_pln_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			train_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.train_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
			convoy_equipment_button_right_click = {
				set_temp_variable = { temp_base_cost_track = global.base_unit_cost }
				multiply_temp_variable = { temp_base_cost_track = global.convoy_cost_mult }
				subtract_from_variable = { new_contract_calc_cost = temp_base_cost_track }
				clamp_variable = {
					var = new_contract_calc_cost
					min = 0
				}
			}
		}
	}
}
