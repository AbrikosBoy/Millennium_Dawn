on_actions = {
	# NEPAD Africa
	on_daily_FRA = {
		effect = {
			if = {
				limit = {
					FRA = { has_completed_focus = FRA_NEPAD_development }
					NOT = { has_global_flag = AFRICA_nepad_focus }
					NOT = { has_country_flag = FRA_nepad_cleanup }
					FRA = {
						has_dynamic_modifier = {
							modifier = FRA_idea_nepad_modifier
						}
					}
				}
				every_other_country = {
					limit = {
						is_african_nation = yes
						has_dynamic_modifier = {
							modifier = AFRICA_idea_nepad_modifier
						}
					}
					remove_dynamic_modifier = { modifier = AFRICA_idea_nepad_modifier }
					ingame_update_setup = yes
				}
				FRA = {
					remove_dynamic_modifier = { modifier = FRA_idea_nepad_modifier }
					clear_variable = FRA_nepad_sponsor_weekly_cost
					ingame_update_setup = yes
				}

				# NEPAD Variables
				clear_variable = global.africa_nepad_building_construction_modifier
				clear_variable = global.africa_nepad_education_modifier
				clear_variable = global.africa_nepad_social_care_modifier
				clear_variable = global.africa_nepad_health_cost_modifier
			}
		}
	}

	on_weekly_FRA = {
		effect = {
			if = {
				limit = {
					has_country_flag = FRA_new_frontiers
					has_completed_focus = FRA_the_space_program
				}
				# Passive Reductio
				add_to_variable = { FRA.FRA_peoples_support = -0.1 }
			}
		}
	}
}