on_actions = {
	on_weekly_SOV = {
		effect = {
			#Third Chechen Prepare
			if = {
				limit = {
					NOT = { SOV = { has_country_flag = che_kto_start } }
				}
				set_country_flag = che_kto_start
				set_temp_variable = { modify_radicalche = 70 }
				modify_radicalche_support = yes
				hidden_effect = {
					if = {
						limit = { SOV = { is_ai = yes } }
						country_event = { id = sov_other.39 days = 50 }
					}
					SOV = { country_event = { id = sov_other.26 days = 220 } }
				}
			}
		}
	}

	on_monthly_SOV = {
		effect = {
			if = {
				limit = {
					has_war = yes
					surrender_progress > 0.8
				}

				country_event = { id = sov_dissolution.1 days = 1 }
			}
		}
	}
}