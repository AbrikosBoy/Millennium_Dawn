#	Example:
#
#	example_trigger = {
#		tag = GER
#		is_ai = no
#	}
#
#
#	In a script file:
#
#	trigger = {
#		exampel_trigger = yes
#	}
#

is_in_africa = {
	capital_scope = { is_on_continent = africa }
}

is_in_asia = {
	capital_scope = { is_on_continent = asia }
}

is_in_europe = {
	capital_scope = { is_on_continent = europe }
}

is_in_south_america = {
	capital_scope = { is_on_continent = south_america }
}

is_in_north_america = {
	capital_scope = { is_on_continent = north_america }
}

is_in_the_americas = {
	OR = {
		capital_scope = { is_on_continent = north_america }
		capital_scope = { is_on_continent = south_america }
	}
}

is_in_the_middle_east = {
	capital_scope = { is_on_continent = middle_east }
}

is_in_oceania = {
	capital_scope = { is_on_continent = oceania }
}

PREV_is_not_on_same_continent = {
	OR = {
		AND = {
			NOT = { capital_scope = { is_on_continent = europe } }
			PREV = { capital_scope = { is_on_continent = europe } }
		}
		AND = {
			NOT = { capital_scope = { is_on_continent = africa } }
			PREV = { capital_scope = { is_on_continent = africa } }
		}
		AND = {
			NOT = { capital_scope = { is_on_continent = asia } }
			PREV = { capital_scope = { is_on_continent = asia } }
		}
		AND = {
			NOT = { capital_scope = { is_on_continent = south_america } }
			PREV = { capital_scope = { is_on_continent = south_america } }
		}
		AND = {
			NOT = { capital_scope = { is_on_continent = north_america } }
			PREV = { capital_scope = { is_on_continent = north_america } }
		}
		AND = {
			NOT = { capital_scope = { is_on_continent = middle_east } }
			PREV = { capital_scope = { is_on_continent = middle_east } }
		}
		AND = {
			NOT = { capital_scope = { is_on_continent = australia } }
			PREV = { capital_scope = { is_on_continent = australia } }
		}
	}
}

PREV_is_on_same_continent = {
	OR = {
		AND = {
			capital_scope = { is_on_continent = europe }
			PREV = { capital_scope = { is_on_continent = europe } }
		}
		AND = {
			capital_scope = { is_on_continent = africa }
			PREV = { capital_scope = { is_on_continent = africa } }
		}
		AND = {
			capital_scope = { is_on_continent = asia }
			PREV = { capital_scope = { is_on_continent = asia } }
		}
		AND = {
			capital_scope = { is_on_continent = south_america }
			PREV = { capital_scope = { is_on_continent = south_america } }
		}
		AND = {
			capital_scope = { is_on_continent = north_america }
			PREV = { capital_scope = { is_on_continent = north_america } }
		}
		AND = {
			capital_scope = { is_on_continent = middle_east }
			PREV = { capital_scope = { is_on_continent = middle_east } }
		}
		AND = {
			capital_scope = { is_on_continent = australia }
			PREV = { capital_scope = { is_on_continent = australia } }
		}
	}
}
