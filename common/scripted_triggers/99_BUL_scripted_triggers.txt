# Author(s): LordBogdanoff

BUL_targeted_country_has_more_than_65_percent_influence = {
	check_variable = { influence_array^0 = BUL }
	check_variable = { influence_array_val^0 > 65.999 }
}

BUL_targeted_country_has_more_than_45_percent_influence = {
	check_variable = { influence_array^0 = BUL }
	check_variable = { influence_array_val^0 > 45.999 }
}