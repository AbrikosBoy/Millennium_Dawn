PLPC_is_on = {
	has_game_rule = {
		rule = allow_player_led_PC
		option = yes
	}
}

PLPC_is_off = {
	has_game_rule = {
		rule = allow_player_led_PC
		option = no
	}
}

CPC_is_on = {
	has_game_rule = {
		rule = allow_chaotic_PC
		option = yes
	}
}

CPC_is_off = {
	has_game_rule = {
		rule = allow_chaotic_PC
		option = no
	}
}

CPC_off_western = {
	ROOT = { has_government = democratic }
	FROM = { has_government = democratic }
}

CPC_off_emerging = {
	ROOT = { has_government = communism }
	FROM = { has_government = communism }
}

CPC_off_salafist = {
	ROOT = { has_government = fascism }
	FROM = { has_government = fascism }
}

CPC_off_non_aligned = {
	ROOT = { has_government = neutrality }
	FROM = { has_government = neutrality }
}

CPC_off_nationalist = {
	ROOT = { has_government = nationalist }
	FROM = { has_government = nationalist }
}