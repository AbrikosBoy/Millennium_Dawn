opinion_modifiers = {

	FRA_aze_supports_our_enemy = {
		value = -50
		decay = -1
	}

	FRA_antagonize_syria_opinion = {
		value = -25
		decay = -1
	}

	FRA_haiti_demands_reparations_opinion = {
		value = -25
		decay = -1
	}

	FRA_repealed_the_ordinance_opinion = {
		value = 25
		decay = -1
	}

	FRA_francophone_solidarity_opinion = {
		value = 25
		decay = -1
	}

	FRA_supported_nepad_development_opinion = {
		value = 15
		decay = -1
	}
}
