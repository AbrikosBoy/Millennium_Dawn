ROM_romarm_tank_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_ROMARM
	name = ROM_romarm_tank_manufacturer
	include = generic_tank_equipment_organization
}

ROM_mfa_mizil_tank_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_MFA_Mizil
	name = ROM_mfa_mizil_tank_manufacturer
	include = generic_tank_equipment_organization
}

ROM_ratmil_materiel_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_ROMARM
	name = ROM_ratmil_materiel_manufacturer
	include = generic_tank_equipment_organization
}

ROM_iar_tank_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_IAR
	name = ROM_iar_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

ROM_avioane_craiova_aircraft_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_Avioane_Craiova
	name = ROM_avioane_craiova_aircraft_manufacturer
	include = generic_air_equipment_organization
}

ROM_aerostar_aircraft_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_Aerostar
	name = ROM_aerostar_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization
}

ROM_galati_shipyard_naval_manufacturer = {
	allowed = { original_tag = ROM }
	icon = GFX_idea_Damen_Galati
	name = ROM_galati_shipyard_naval_manufacturer
	include = generic_naval_light_equipment_organization
}