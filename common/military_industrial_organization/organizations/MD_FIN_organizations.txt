FIN_patria_tank_manufacturer = {
	allowed = { original_tag = FIN }
	icon = GFX_idea_Patria
	name = FIN_patria_tank_manufacturer
	include = generic_tank_equipment_organization
}

FIN_sako_materiel_manufacturer = {
	allowed = { original_tag = FIN }
	icon = GFX_idea_SAKO
	name = FIN_sako_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

FIN_patria_materiel_manufacturer = {
	allowed = { original_tag = FIN }
	icon = GFX_idea_Patria
	name = FIN_patria_materiel_manufacturer
	include = generic_infantry_equipment_organization
}


FIN_meyer_turku_naval_manufacturer = {
	allowed = { original_tag = FIN }
	icon = GFX_idea_Meyer_Turku
	name = FIN_meyer_turku_naval_manufacturer
	include = generic_naval_equipment_organization
}
