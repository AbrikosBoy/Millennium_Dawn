CRO_djuro_djakovic_tank_manufacturer = {
	allowed = { original_tag = CRO }
	icon = GFX_idea_Djuro_Djakovic_CRO
	name = CRO_djuro_djakovic_tank_manufacturer
	include = generic_tank_equipment_organization
}

CRO_hs_produkt_materiel_manufacturer = {
	allowed = { original_tag = CRO }
	icon = GFX_idea_HS_Produkt
	name = CRO_hs_produkt_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

CRO_kraljevica_shipyard_naval_manufacturer = {
	allowed = { original_tag = CRO }
	icon = GFX_idea_Kraljevica_Shipyard_1
	name = CRO_kraljevica_shipyard_naval_manufacturer
	include = generic_naval_equipment_organization
}
