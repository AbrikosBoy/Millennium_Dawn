﻿add_namespace = SouthSudan
add_namespace = SouthSudanNews

### South Sudan civil war cleanup events ###

#Give the winner SSU cosmetic tag
country_event = {
	id = SouthSudan.0

	hidden = yes
	fire_only_once = yes

	trigger = {
		OR = {
			AND = {
				tag = SSU
				NOT = {
					country_exists = AGF
				}
			}
			AND = {
				tag = AGF
				NOT = {
					country_exists = SSU
				}
			}
		}
		NOT = { has_global_flag = SouthSudan_Civil_War_Over  }
	}

	mean_time_to_happen = {
		days = 2
	}

	immediate = {
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.0 executed"
		#Civil war is over
		set_global_flag = SouthSudan_Civil_War_Over
		#Clear core status (normal states
		225 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		226 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		227 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		924 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		#Make the winner Central African Republic
		if = {
			limit = {
				tag = AGF
			}
			set_cosmetic_tag = SSU
			set_country_flag = dynamic_flag
			set_country_flag = dynamic_rebel_flag
			remove_ideas = Non_State_Actor
		}
		add_state_core = 225
		add_state_core = 226
		add_state_core = 227
		add_state_core = 924
		set_capital = { state = 227 }

		#Special States
		##Kafia Kingi (held by Sudan, core of South Sudan)
		if = {
			limit = {
				923 = {
					OR = {
						is_core_of = SSU
						is_core_of = AGF
					}
				}
			}
			923 = {
				remove_core_of = SSU
				remove_core_of = AGF
			}
			add_state_core = 923
		}
		if = {
			limit = {
				923 = {
					OR = {
						is_claimed_by = SSU
						is_claimed_by = AGF
					}
				}
			}
			923 = {
				remove_claim_by = SSU
				remove_claim_by = AGF
			}
			add_state_claim = 923
		}
		##Ilemi (held by Kenya, core of South Sudan)
		if = {
			limit = {
				925 = {
					OR = {
						is_core_of = SSU
						is_core_of = AGF
					}
				}
			}
			925 = {
				remove_core_of = SSU
				remove_core_of = AGF
			}
			add_state_core = 925
		}
		if = {
			limit = {
				925 = {
					OR = {
						is_claimed_by = SSU
						is_claimed_by = AGF
					}
				}
			}
			925 = {
				remove_claim_by = SSU
				remove_claim_by = AGF
			}
			add_state_claim = 925
		}
		##Napak (held by Kenya, claimed by South Sudan)
		if = {
			limit = {
				926 = {
					OR = {
						is_core_of = SSU
						is_core_of = AGF
					}
				}
			}
			926 = {
				remove_core_of = SSU
				remove_core_of = AGF
			}
			add_state_core = 926
		}
		if = {
			limit = {
				926 = {
					OR = {
						is_claimed_by = SSU
						is_claimed_by = AGF
					}
				}
			}
			926 = {
				remove_claim_by = SSU
				remove_claim_by = AGF
			}
			add_state_claim = 926
		}
		#Abyei (held by Sudan, core of South Sudan)
		if = {
			limit = {
				927 = {
					OR = {
						is_core_of = SSU
						is_core_of = AGF
					}
				}
			}
			927 = {
				remove_core_of = SSU
				remove_core_of = AGF
			}
			add_state_core = 927
		}
		if = {
			limit = {
				927 = {
					OR = {
						is_claimed_by = SSU
						is_claimed_by = AGF
					}
				}
			}
			927 = {
				remove_claim_by = SSU
				remove_claim_by = AGF
			}
			add_state_claim = 927
		}
		hidden_effect = { news_event = { id = SouthSudanNews.1 hours = 6 } }
	}

}

#Recreate South Sudan in case it has been annexed
country_event = {
	id = SouthSudan.1
	hidden = yes
	is_triggered_only = yes
	trigger = {
		NOT = { has_global_flag = SSU_peace_agreement_accepted }
	}

	immediate = {
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.1 executed"
		#Clear core status
		225 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		226 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		227 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		924 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		923 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		923 = {
			remove_claim_by = SSU
			remove_claim_by = AGF
		}
		925 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		925 = {
			remove_claim_by = SSU
			remove_claim_by = AGF
		}
		926 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		926 = {
			remove_claim_by = SSU
			remove_claim_by = AGF
		}
		927 = {
			remove_core_of = SSU
			remove_core_of = AGF
		}
		927 = {
			remove_claim_by = SSU
			remove_claim_by = AGF
		}
		SSU = {
			add_state_core = 225
			add_state_core = 226
			add_state_core = 227
			add_state_core = 924
			add_state_core = 923
			add_state_core = 925
			add_state_core = 926
			add_state_core = 927
			set_capital = { state = 227 }
		}
	}
}

news_event = {
	id = SouthSudanNews.1
	title = SouthSudanNews.1.t
	desc = {
		text = SouthSudanNews.1.SSU.d
		trigger = { FROM = { original_tag = SSU } }
	}
	desc = {
		text = SouthSudanNews.1.AGF.d
		trigger = { FROM = { original_tag = AGF } }
	}
	picture = GFX_news_south_sudan_civil_war

	major = yes

	is_triggered_only = yes

	option = {
		name = SouthSudanNews.1.a
		log = "[GetDateText]: [THIS.GetName]: SouthSudanNews.1.a executed"
	}

}

# The Comprehensive Peace Agreements
country_event = {
	id = SouthSudan.2
	title = SouthSudan.2.t
	desc = SouthSudan.2.d
	picture = GFX_political_activist
	is_triggered_only = yes
	trigger = {
		original_tag = SSU
		has_war_with = SUD
		NOT = { has_global_flag = Sudan_Civil_War_Over }
	}
	immediate = {
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.2 executed"
		hidden_effect = {
			set_global_flag = SSU_peace_deal_event_triggered
		}
	}

	# Offer Sudan Ceasefire
	option = {
		name = SouthSudan.2.a
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.2.a executed"

		SUD = { country_event = { id = SouthSudan.3 days = 14 random_days = 14 } }
		ai_chance = {
			factor = 1
			modifier = {
				factor = 2
				check_variable = { interest_rate > 9.99 }
			}
			modifier = {
				is_historical_focus_on = yes
				factor = 20
			}
		}
	}

	# War by Jingo!
	option = {
		name = SouthSudan.2.b
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.2.b executed"

		add_timed_idea = {
			idea = revitalized_war_effort
			days = 365
		}
		ai_chance = {
			factor = 2
		}
	}
}

# Sudan's response
country_event = {
	id = SouthSudan.3
	title = SouthSudan.3.t
	desc = SouthSudan.3.d
	picture = GFX_political_activist
	is_triggered_only = yes
	trigger = {
		has_start_date < 2016.1.1
		has_war_with = SSU
		NOT = { has_global_flag = Sudan_Civil_War_Over }
	}

	# Accept Sudan's Offer
	option = {
		name = SouthSudan.3.a
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.3.a executed"
		effect_tooltip = {
			SSU = { white_peace = SUD }
		}
		hidden_effect = {
			SSU = {
				set_country_flag = SSU_ceasefire_accepted
			}
		}
		SSU = { country_event = { id = SouthSudan.4 days = 14 random_days = 14 } }
		ai_chance = {
			factor = 5
			modifier = {
				factor = 5
				is_historical_focus_on = yes
			}
		}
	}

	# Deny the Ceasefire
	option = {
		name = SouthSudan.3.b
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.3.b executed"
		add_timed_idea = {
			idea = revitalized_war_effort
			days = 365
		}
		effect_tooltip = {
			SSU = {
				add_timed_idea = {
					idea = revitalized_war_effort
					days = 365
				}
			}
		}
		hidden_effect = {
			SSU = {
				set_country_flag = SSU_ceasefire_denied
			}
		}
		SSU = { country_event = { id = SouthSudan.4 days = 14 random_days = 14 } }
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				is_historical_focus_on = yes
			}
		}
	}
}

# Response event
country_event = {
	id = SouthSudan.4
	title = {
		text = SouthSudan.4.t1
		trigger = {
			has_country_flag = SSU_ceasefire_accepted
		}
	}
	title = {
		text = SouthSudan.4.t2
		trigger = {
			has_country_flag = SSU_ceasefire_denied
		}
	}
	desc = {
		text = SouthSudan.4.d1
		trigger = {
			has_country_flag = SSU_ceasefire_accepted
		}
	}
	desc = {
		text = SouthSudan.4.d2
		trigger = {
			has_country_flag = SSU_ceasefire_denied
		}
	}
	picture = GFX_political_activist
	is_triggered_only = yes
	trigger = {
		has_start_date < 2016.1.1
		SUD = { has_war_with = SSU }
		NOT = { has_global_flag = SouthSudan_Civil_War_Over }
	}

	# Ceasefire Accepted
	option = {
		name = SouthSudan.4.a
		trigger = {
			has_country_flag = SSU_ceasefire_accepted
		}
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.4.a executed"
		add_stability = 0.05
		add_war_support = 0.05
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes
		white_peace = FROM
		diplomatic_relation = {
			country = FROM
			relation = non_aggression_pact
			active = yes
		}

		hidden_effect = {
			SUD = {
				add_ideas = defence_02
				if = { limit = { is_ai = yes }
					add_timed_idea = {
						idea = post_war_recovery_effort
						days = 365
					}
					set_temp_variable = { debt_change = SUD.debt }
					multiply_temp_variable = { debt_change = -0.20 }
					modify_debt_effect = yes
				}
			}
			SSU = {
				add_ideas = defence_02
				if = { limit = { is_ai = yes }
					add_timed_idea = {
						idea = post_war_recovery_effort
						days = 365
					}
					set_temp_variable = { debt_change = SSU.debt }
					multiply_temp_variable = { debt_change = -0.20 }
					modify_debt_effect = yes
				}
			}
		}
		news_event = SouthSudanNews.2
	}

	# The Ceasefire Denied
	option = {
		name = SouthSudan.4.b
		trigger = {
			has_country_flag = SSU_ceasefire_denied
		}
		log = "[GetDateText]: [THIS.GetName]: SouthSudan.4.b executed"

		add_timed_idea = {
			idea = revitalized_war_effort
			days = 365
		}
		effect_tooltip = {
			SUD = {
				add_timed_idea = {
					idea = revitalized_war_effort
					days = 365
				}
			}
		}

		news_event = SouthSudanNews.2
	}
}

# Peace in South Sudan
news_event = {
	id = SouthSudanNews.2
	title = {
		text = SouthSudanNews.2.t1
		trigger = {
			SSU = { has_country_flag = SSU_ceasefire_accepted }
		}
	}
	title = {
		text = SouthSudanNews.2.t2
		trigger = {
			SSU = { has_country_flag = SSU_ceasefire_denied }
		}
	}
	desc = {
		text = SouthSudanNews.2.d1
		trigger = {
			SSU = { has_country_flag = SSU_ceasefire_accepted }
		}
	}
	desc = {
		text = SouthSudanNews.2.d2
		trigger = {
			SSU = { has_country_flag = SSU_ceasefire_denied }
		}
	}
	picture = GFX_news_press_conference
	is_triggered_only = yes
	major = yes
	trigger = {
		has_start_date < 2016.1.1
	}

	option = {
		name = SouthSudanNews.2.a
		log = "[GetDateText]: [THIS.GetName]: SouthSudanNews.4.a executed"

		hidden_effect = {
			if = { limit = { SSU = { has_country_flag = SSU_ceasefire_accepted } }
				set_global_flag = Sudan_Civil_War_Over
				set_global_flag = SSU_peace_agreement_accepted
			}
			else = {
				set_global_flag = SSU_peace_agreement_denied
			}
			SSU = {
				clr_country_flag = SSU_ceasefire_accepted
				clr_country_flag = SSU_ceasefire_accepted
			}

		}
	}
}